#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include "sqlite3.h"
#include "interbbs2.h"

tIBInfo ibbs;
int delete_bad = 0;
char *bad_path = NULL;

void create_report() {
    sqlite3 *db = NULL;
    sqlite3_stmt *res = NULL;
    FILE *fptr, *fptr2;
    time_t timestamp;
    struct tm *thetime;
    int rc;
    char c;
    char hostver[6];
    int hostgameid;
    time_t hsttime;
    struct tm *hsttimetm;

    int outofdatenodes = 0;
    int wronggameidnodes = 0;

    int thisnodeoutofdate = 0;
    int thisnodewronggameid = 0;
    int inactivenodes = 0;


    const char* sql = "SELECT packetver, node, systemname, date, gameid, hostgameid, hostpacketver FROM ibbs_info WHERE node = ?";

    fptr = fopen("report.txt", "w");
    if (!fptr) {
        return;
    }

    rc = sqlite3_open("ibbs_info.db", &db);
    if (rc) {
        // Error opening the database
        fprintf(stderr, "Error opening ibbs_info.db: %s\n", sqlite3_errmsg(db));
        fclose(fptr);
        return;
    }

    sqlite3_busy_timeout(db, 5000);

    for (int i = 0; i < ibbs.otherNodeCount; i++) {

        rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
        if (rc != SQLITE_OK) {
            fprintf(stderr, "Error preparing statement: %s\n", sqlite3_errmsg(db));
            return;
        }

        sqlite3_bind_int(res, 1, ibbs.otherNodes[i]->nodeNumber);
    

        if (sqlite3_step(res) == SQLITE_ROW) {
            strncpy(hostver, (const char *)sqlite3_column_text(res, 6), 6);
            hostgameid = sqlite3_column_int(res, 5);
        }
    }

    fptr2 = fopen("report_header.txt", "r");
    if (fptr2) {
        c = fgetc(fptr2);
        while (!feof(fptr2)) {
            if (c == '|') {
                c = fgetc(fptr2);
                switch (c) {
                    case 'D':
                        /* print date */
                        hsttime = time(NULL);
                        hsttimetm = localtime(&hsttime);

                        fprintf(fptr, "%04d-%02d-%02d  %02d:%02d",  hsttimetm->tm_year + 1900, hsttimetm->tm_mon + 1, hsttimetm->tm_mday, hsttimetm->tm_hour, hsttimetm->tm_min);
                        break;
                    case 'V':
                        fprintf(fptr, "%s", hostver);
                        break;
                    case 'G':
                        fprintf(fptr, "%5d", hostgameid);
                        break;
                    default:
                        fprintf(fptr, "|%c", c);
                        break;
                }
            } else {
                fprintf(fptr, "%c", c);
            }
            c = fgetc(fptr2);
        }
        fclose(fptr2);
    }

    fprintf(fptr, "+-----------------------------------------------------------------------------+\n");
    fprintf(fptr, "|NODE | SYSTEM NAME                      |VERSION|GAME ID| LAST PACKET        |\n");
    fprintf(fptr, "+-----------------------------------------------------------------------------+\n");
    
    sqlite3_finalize(res);

    for (int i = 1; i < ibbs.otherNodeCount; i++) {
        rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
        if (rc != SQLITE_OK) {
            fprintf(stderr, "Error preparing statement: %s\n", sqlite3_errmsg(db));
            return;
        }

        sqlite3_bind_int(res, 1, ibbs.otherNodes[i]->nodeNumber);
    

        if (sqlite3_step(res) == SQLITE_ROW) {
            timestamp = sqlite3_column_int(res, 3);
            thetime = localtime(&timestamp);

            thisnodeoutofdate = 0;

            if (strcmp((const char *)sqlite3_column_text(res, 0), hostver) != 0) {
                outofdatenodes++;
                thisnodeoutofdate = 1;
            }
            
            thisnodewronggameid = 0;
            if (sqlite3_column_int(res, 4) != hostgameid) {
                wronggameidnodes++;
                thisnodewronggameid = 1;
            }

            if (thisnodeoutofdate && thisnodewronggameid) {
                fprintf(fptr, "| %3d | %-32.32s |*%-5.5s*|*%5d*| %04d-%02d-%02d  %02d:%02d  |\n", sqlite3_column_int(res, 1), sqlite3_column_text(res, 2), sqlite3_column_text(res, 0), sqlite3_column_int(res, 4), thetime->tm_year + 1900, thetime->tm_mon + 1, thetime->tm_mday, thetime->tm_hour, thetime->tm_min);
            } else if (thisnodeoutofdate) {
                fprintf(fptr, "| %3d | %-32.32s |*%-5.5s*| %5d | %04d-%02d-%02d  %02d:%02d  |\n", sqlite3_column_int(res, 1), sqlite3_column_text(res, 2), sqlite3_column_text(res, 0), sqlite3_column_int(res, 4), thetime->tm_year + 1900, thetime->tm_mon + 1, thetime->tm_mday, thetime->tm_hour, thetime->tm_min);
            } else if (thisnodewronggameid) {
                fprintf(fptr, "| %3d | %-32.32s | %-5.5s |*%5d*| %04d-%02d-%02d  %02d:%02d  |\n", sqlite3_column_int(res, 1), sqlite3_column_text(res, 2), sqlite3_column_text(res, 0), sqlite3_column_int(res, 4), thetime->tm_year + 1900, thetime->tm_mon + 1, thetime->tm_mday, thetime->tm_hour, thetime->tm_min);
            } else {
                fprintf(fptr, "| %3d | %-32.32s | %-5.5s | %5d | %04d-%02d-%02d  %02d:%02d  |\n", sqlite3_column_int(res, 1), sqlite3_column_text(res, 2), sqlite3_column_text(res, 0), sqlite3_column_int(res, 4), thetime->tm_year + 1900, thetime->tm_mon + 1, thetime->tm_mday, thetime->tm_hour, thetime->tm_min);
            }

        } else {
            fprintf(fptr, "| %3d | %-32.32s | ????? | ????? | NEVER!             |\n", ibbs.otherNodes[i]->nodeNumber, ibbs.otherNodes[i]->name);
            inactivenodes++;
        }
        sqlite3_finalize(res);
    }
    fprintf(fptr, "+-----------------------------------------------------------------------------+\n");

    fprintf(fptr, "Nodes running out of date game: %d\n", outofdatenodes);
    fprintf(fptr, "Nodes with incorrect game id: %d\n", wronggameidnodes);
    fprintf(fptr, "Inactive Nodes: %d\n", inactivenodes);
    fclose(fptr);
    sqlite3_close(db);
}


int main(int argc, char **argv) {
    IBReadConfig(&ibbs, "GAL-IBBS.CFG");
    create_report();
    return 0;
}
