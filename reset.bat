if exist users.db3 (
    del users.db3
)
if exist msgs.db3 (
    del msgs.db3
)
if exist interbbs.db3 (
    del interbbs.db3
)

if exist game_id.dat (
    del game_id.dat
)
if exist ibbs_winner.dat (
    del ibbs_winner.dat
)

if exist ibbs_info.db (
    del ibbs_info.db
)

if exist scores.ans (
    del scores.ans
)

if exist ibbs_scores.ans (
    del ibbs_scores.ans
)

if exist scores.asc (
    del scores.asc
)

if exist ibbs_scores.asc (
    del ibbs_scores.asc
)
