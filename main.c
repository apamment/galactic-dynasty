#include <MagiDoor.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sqlite3.h>
#include <ctype.h>
#include <limits.h>
#ifndef _MSC_VER
#include <unistd.h>
#endif
#include <stdint.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <inttypes.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#if defined(_MSC_VER) || defined(WIN32)
#include <winsock2.h>
#include <windows.h>
#define snprintf _snprintf
#define strcasecmp _stricmp
#define strncasecmp _strnicmp
#define htonll(x) ((((x)&0xff00000000000000ull) >> 56) | (((x)&0x00ff000000000000ull) >> 40) | (((x)&0x0000ff0000000000ull) >> 24) | (((x)&0x000000ff00000000ull) >> 8) | (((x)&0x00000000ff000000ull) << 8) | (((x)&0x0000000000ff0000ull) << 24) | (((x)&0x000000000000ff00ull) << 40) | (((x)&0x00000000000000ffull) << 56))
#define ntohll htonll
#else
#include <arpa/inet.h>
#if defined(__linux__)
#include <endian.h>
#define ntohll(x) be64toh(x)
#define htonll(x) htobe64(x)
#elif defined(__FreeBSD__) || defined(__NetBSD__)
#include <sys/endian.h>
#define ntohll(x) be64toh(x)
#elif defined(__OpenBSD__)
#include <sys/types.h>
#define ntohll(x) betoh64(x)
#endif
#endif
#include "interbbs2.h"
#include "inih/ini.h"

#if MAGIDOOR_VERSION_MAJOR < 1 || MAGIDOOR_VERSION_MINOR < 3
#error Please update magidoor.
#endif

#define VERSION_MAJOR 1
#define VERSION_MINOR 4
#define VERSION_MICRO 0

#ifndef VERSION_TYPE
#define VERSION_TYPE "dev"
#endif

#define TURNS_PER_DAY 5
#define TURNS_IN_PROTECTION 0



void dolog(char *fmt, ...);


int turns_per_day;
int turns_in_protection;
int full;
char *log_path;
char *bad_path;
int delete_bad;

tIBInfo InterBBSInfo;
int interBBSMode;

typedef struct player
{
	int id;
	char bbsname[256];
	char gamename[17];

	uint32_t troops;
	uint32_t generals;
	uint32_t fighters;
	uint32_t defence_stations;
	uint32_t spies;

	uint32_t population;
	uint32_t food;
	uint64_t credits;

	uint64_t planets_food;
	uint64_t planets_ore;
	uint64_t planets_industrial;
	uint64_t planets_military;
	uint64_t planets_urban;

	uint32_t command_ship;

	uint32_t turns_left;
	time_t last_played;
	uint64_t last_score;
	uint32_t total_turns;
	int64_t bank_balance;
	uint32_t sprockets;
	uint32_t ore;
} player_t;

player_t *gPlayer;

typedef struct message
{
	int id;
	char to[17];
	char from[17];
	int addr;
	int system;
	time_t date;
	int seen;
	char body[256];
} message_t;

#pragma pack(push, 1)

typedef struct ibbsmsg
{
	int32_t type;
	uint32_t from;
	char player_name[17];
	char victim_name[17];
	uint64_t score;
	uint32_t troops;
	uint32_t generals;
	uint32_t fighters;
	uint64_t plunder_credits;
	uint32_t plunder_food;
	uint32_t plunder_people;
	char message[256];
	uint32_t created;
	uint32_t turns_per_day;
	uint32_t turns_in_protection;
} ibbsmsg_t;
#pragma pack(pop)

typedef struct ibbsscore
{
	char player_name[17];
	char bbs_name[40];
	uint64_t score;
	int winner;
} ibbsscores_t;

uint64_t calculate_score(player_t *player);
player_t *load_player_gn(const char *gamename);
void send_message(player_t *to, player_t *from, char *body);

void msg2ne(ibbsmsg_t *msg)
{
	msg->type = htonl(msg->type);
	msg->from = htonl(msg->from);
	msg->score = htonll(msg->score);
	msg->troops = htonl(msg->troops);
	msg->generals = htonl(msg->generals);
	msg->fighters = htonl(msg->fighters);
	msg->plunder_credits = htonll(msg->plunder_credits);
	msg->plunder_food = htonl(msg->plunder_food);
	msg->plunder_people = htonl(msg->plunder_people);
	msg->created = htonl(msg->created);
	msg->turns_per_day = htonl(msg->turns_per_day);
	msg->turns_in_protection = htonl(msg->turns_in_protection);
}

void msg2he(ibbsmsg_t *msg)
{
	msg->type = ntohl(msg->type);
	msg->from = ntohl(msg->from);
	msg->score = ntohll(msg->score);
	msg->troops = ntohl(msg->troops);
	msg->generals = ntohl(msg->generals);
	msg->fighters = ntohl(msg->fighters);
	msg->plunder_credits = ntohll(msg->plunder_credits);
	msg->plunder_food = ntohl(msg->plunder_food);
	msg->plunder_people = ntohl(msg->plunder_people);
	msg->created = ntohl(msg->created);
	msg->turns_per_day = ntohl(msg->turns_per_day);
	msg->turns_in_protection = ntohl(msg->turns_in_protection);
}


int open_msgs_database(sqlite3 **db) {
    int rc;
    char *err_msg = NULL;

    static const char *create_sql = "CREATE TABLE IF NOT EXISTS messages ("
        "'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "'recipient' TEXT,"
        "'from' TEXT,"
        "'system' INTEGER,"
        "'date' INTEGER,"
        "'seen' INTEGER,"
        "'body' TEXT);";

    if (sqlite3_open("msgs.db3", db) != SQLITE_OK) {
        return 0;
    }

    sqlite3_busy_timeout(*db, 5000);

	rc = sqlite3_exec(*db, create_sql, 0, 0, &err_msg);
    if (rc != SQLITE_OK) {
        dolog("Error opening msgs database: %s", sqlite3_errmsg(*db));
		sqlite3_free(err_msg);
		sqlite3_close(*db);
		return 0;
	}
    return 1;
}

int open_users_database(sqlite3 **db) {
    int rc;
    char *err_msg = NULL;

    static const char *create_sql = "CREATE TABLE IF NOT EXISTS users ("
        "'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
        "'bbsname' TEXT,"
        "'gamename' TEXT,"
        "'troops' INTEGER,"
        "'generals' INTEGER,"
        "'fighters' INTEGER,"
        "'defence_stations' INTEGER,"
        "'population' INTEGER,"
        "'food' INTEGER,"
        "'credits' INTEGER,"
        "'planets_food' INTEGER,"
        "'planets_ore' INTEGER,"
        "'planets_industrial' INTEGER,"
        "'planets_military' INTEGER,"
        "'planets_urban' INTEGER,"
        "'command_ship' INTEGER,"
        "'turns_left' INTEGER,"
        "'last_played' INTEGER,"
        "'spies' INTEGER,"
        "'last_score' INTEGER,"
        "'total_turns' INTEGER,"
        "'bank_balance' INTEGER,"
        "'sprockets' INTEGER,"
        "'ore' INTEGER);";

    if (sqlite3_open("users.db3", db) != SQLITE_OK) {
        return 0;
    }

    sqlite3_busy_timeout(*db, 5000);

	rc = sqlite3_exec(*db, create_sql, 0, 0, &err_msg);
    if (rc != SQLITE_OK) {
        dolog("Error opening users database: %s", sqlite3_errmsg(*db));
		sqlite3_free(err_msg);
		sqlite3_close(*db);
		return 0;
	}
    return 1;
}

int open_interbbs_database(sqlite3 **db) {
    int rc;
    char *err_msg = NULL;

    static const char *create_1_sql = "CREATE TABLE IF NOT EXISTS scores ("
        "'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
        "'address' INTEGER,"
        "'gamename' VARCHAR,"
        "'score' INTEGER,"
        "'last' INTEGER);";

    static const char *create_2_sql = "CREATE TABLE IF NOT EXISTS messages ("
        "'id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
        "'recipient' VARCHAR,"
        "'from' VARCHAR,"
        "'address' INTEGER,"
        "'date' INTEGER,"
        "'seen' INTEGER,"
        "'body' VARCHAR);";

    static const char *create_3_sql = "CREATE TABLE IF NOT EXISTS recon ("
        "'address' INTEGER,"
        "'last' INTEGER);";

    if (sqlite3_open("interbbs.db3", db) != SQLITE_OK) {
        return 0;
    }

    sqlite3_busy_timeout(*db, 5000);

	rc = sqlite3_exec(*db, create_1_sql, 0, 0, &err_msg);
    if (rc != SQLITE_OK) {
        dolog("Error opening interbbs database: %s", sqlite3_errmsg(*db));
		sqlite3_free(err_msg);
		sqlite3_close(*db);
		return 0;
	}

	rc = sqlite3_exec(*db, create_2_sql, 0, 0, &err_msg);
    if (rc != SQLITE_OK) {
        dolog("Error opening interbbs database: %s", sqlite3_errmsg(*db));
		sqlite3_free(err_msg);
		sqlite3_close(*db);
		return 0;
	}
	rc = sqlite3_exec(*db, create_3_sql, 0, 0, &err_msg);
    if (rc != SQLITE_OK) {
        dolog("Error opening interbbs database: %s", sqlite3_errmsg(*db));
		sqlite3_free(err_msg);
		sqlite3_close(*db);
		return 0;
	}
    return 1;
}

uint64_t gd_get_uint64(uint64_t quickmax) {
    char c;
    uint64_t retval = 0;

    static char lastc = 'x';


    while (1) {
        c = md_getc();
        if (c == '\n' || c == '\0') {
            lastc = c;
            if (lastc == '\r') {
                continue;
            } else {
                return retval;
            }
        }
        if (c == '\r') {
            lastc = c;
            return retval;
        } 
        if (c == '\b' || c == 127) {
            if (retval > 0) {
                retval /= 10;
                md_printf("\b \b");

            }
            lastc = c;
            continue;
        }
        if (c == '>' && retval == 0) {
            if (quickmax > 0) {
                md_printf("%" PRIu64, quickmax);
                lastc = c;
                retval = quickmax;
            }
        }
        if (c >= '0' && c <= '9') {
            retval = retval * 10 + (c - '0');
            md_putchar(c);
        }
        lastc = c;
    }    
}

void dolog(char *fmt, ...)
{
	char buffer[PATH_MAX];
	struct tm *time_now;
	time_t timen;
	FILE *logfptr;

	timen = time(NULL);

	time_now = localtime(&timen);
	if (log_path != NULL)
	{
		snprintf(buffer, PATH_MAX, "%s%s%04d%02d%02d.log", log_path, PATH_SEP, time_now->tm_year + 1900, time_now->tm_mon + 1, time_now->tm_mday);
	}
	else
	{
		snprintf(buffer, PATH_MAX, "%04d%02d%02d.log", time_now->tm_year + 1900, time_now->tm_mon + 1, time_now->tm_mday);
	}
	logfptr = fopen(buffer, "a");
	if (!logfptr)
	{
		return;
	}
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buffer, 512, fmt, ap);
	va_end(ap);

	fprintf(logfptr, "%02d:%02d:%02d %s\n", time_now->tm_hour, time_now->tm_min, time_now->tm_sec, buffer);

	fclose(logfptr);
}

static int handler(void *user, const char *section, const char *name,
				   const char *value)
{
	if (strcasecmp(section, "main") == 0)
	{
		if (strcasecmp(name, "turns per day") == 0)
		{
			turns_per_day = atoi(value);
		}
		else if (strcasecmp(name, "turns in protection") == 0)
		{
			turns_in_protection = atoi(value);
		}
		else if (strcasecmp(name, "log path") == 0)
		{
			log_path = strdup(value);
#if defined(_MSC_VER) || defined(WIN32)
			if (log_path[strlen(log_path) - 1] == '\\')
			{
#else
			if (log_path[strlen(log_path) - 1] == '/')
			{
#endif
				log_path[strlen(log_path) - 1] = '\0';
			}
		}
		else if (strcasecmp(name, "bad path") == 0)
		{
			bad_path = strdup(value);
#if defined(_MSC_VER) || defined(WIN32)
			if (bad_path[strlen(bad_path) - 1] == '\\')
			{
#else
			if (bad_path[strlen(bad_path) - 1] == '/')
			{
#endif
				bad_path[strlen(bad_path) - 1] = '\0';
			}
		}
		else if (strcasecmp(name, "delete bad") == 0)
		{
			if (strcasecmp(value, "true") == 0)
			{
				delete_bad = 1;
			}
			else
			{
				delete_bad = 0;
			}
		}
	}
	else if (strcasecmp(section, "interbbs") == 0)
	{
		if (strcasecmp(name, "enabled") == 0)
		{
			if (strcasecmp(value, "true") == 0)
			{
				interBBSMode = 1;
			}
			else
			{
				interBBSMode = 0;
			}
		}
		else if (strcasecmp(name, "system name") == 0)
		{
			strncpy(InterBBSInfo.myNode->name, value, SYSTEM_NAME_CHARS);
			InterBBSInfo.myNode->name[SYSTEM_NAME_CHARS] = '\0';
		}
		else if (strcasecmp(name, "league number") == 0)
		{
			InterBBSInfo.league = atoi(value);
		}
		else if (strcasecmp(name, "node number") == 0)
		{
			InterBBSInfo.myNode->nodeNumber = atoi(value);
		}
		else if (strcasecmp(name, "file inbox") == 0)
		{
			strncpy(InterBBSInfo.myNode->filebox, value, PATH_MAX);
			InterBBSInfo.myNode->filebox[PATH_MAX] = '\0';
		}
		else if (strcasecmp(name, "default outbox") == 0)
		{
			strncpy(InterBBSInfo.defaultFilebox, value, PATH_MAX);
			InterBBSInfo.defaultFilebox[PATH_MAX] = '\0';
		}
	}
	return 1;
}

int lua_getTroops(lua_State *L)
{
	lua_pushnumber(L, gPlayer->troops);
	return 1;
}

int lua_getGenerals(lua_State *L)
{
	lua_pushnumber(L, gPlayer->generals);
	return 1;
}

int lua_getFighters(lua_State *L)
{
	lua_pushnumber(L, gPlayer->fighters);
	return 1;
}

int lua_getDefenceStations(lua_State *L)
{
	lua_pushnumber(L, gPlayer->defence_stations);
	return 1;
}

int lua_getSpies(lua_State *L)
{
	lua_pushnumber(L, gPlayer->spies);
	return 1;
}

int lua_getPopulation(lua_State *L)
{
	lua_pushnumber(L, gPlayer->population);
	return 1;
}

int lua_getFood(lua_State *L)
{
	lua_pushnumber(L, gPlayer->food);
	return 1;
}

int lua_getCredits(lua_State *L)
{
	lua_pushnumber(L, gPlayer->credits);
	return 1;
}

int lua_getPlanets(lua_State *L)
{
	lua_pushnumber(L, gPlayer->planets_food + gPlayer->planets_industrial + gPlayer->planets_military + gPlayer->planets_ore + gPlayer->planets_urban);
	return 1;
}

int lua_inProtection(lua_State *L)
{
	lua_pushnumber(L, turns_in_protection > gPlayer->total_turns);
	return 1;
}

int lua_destroyPlanets(lua_State *L)
{
	uint64_t count = lua_tonumber(L, -1);
	uint64_t total = 0;

	uint64_t eachp = count / 5;

	if (gPlayer->planets_food < eachp)
	{
		total += gPlayer->planets_food;
		gPlayer->planets_food = 0;
	}
	else
	{
		total += eachp;
		gPlayer->planets_food -= eachp;
	}

	if (gPlayer->planets_military < eachp)
	{
		total += gPlayer->planets_military;
		gPlayer->planets_military = 0;
	}
	else
	{
		total += eachp;
		gPlayer->planets_military -= eachp;
	}
	if (gPlayer->planets_urban < eachp)
	{
		total += gPlayer->planets_urban;
		gPlayer->planets_urban = 0;
	}
	else
	{
		total += eachp;
		gPlayer->planets_urban -= eachp;
	}
	if (gPlayer->planets_ore < eachp)
	{
		total += gPlayer->planets_ore;
		gPlayer->planets_ore = 0;
	}
	else
	{
		total += eachp;
		gPlayer->planets_ore -= eachp;
	}
	if (gPlayer->planets_industrial < eachp)
	{
		total += gPlayer->planets_industrial;
		gPlayer->planets_industrial = 0;
	}
	else
	{
		total += eachp;
		gPlayer->planets_industrial -= eachp;
	}

	lua_pushnumber(L, total);

	return 1;
}

int lua_setTroops(lua_State *L)
{
	gPlayer->troops = lua_tonumber(L, -1);
	return 0;
}

int lua_setGenerals(lua_State *L)
{
	gPlayer->generals = lua_tonumber(L, -1);
	return 0;
}

int lua_setFighters(lua_State *L)
{
	gPlayer->fighters = lua_tonumber(L, -1);
	return 0;
}

int lua_setDefenceStations(lua_State *L)
{
	gPlayer->defence_stations = lua_tonumber(L, -1);
	return 0;
}

int lua_setSpies(lua_State *L)
{
	gPlayer->spies = lua_tonumber(L, -1);
	return 0;
}

int lua_setPopulation(lua_State *L)
{
	gPlayer->population = lua_tonumber(L, -1);
	return 0;
}

int lua_setFood(lua_State *L)
{
	gPlayer->food = lua_tonumber(L, -1);
	return 0;
}

int lua_setCredits(lua_State *L)
{
	gPlayer->credits = lua_tonumber(L, -1);
	return 0;
}

int lua_printRed(lua_State *L)
{
	md_printf("`bright red`%s`white`\r\n", (char *)lua_tostring(L, -1));
	return 0;
}

int lua_printYellow(lua_State *L)
{
	md_printf("`bright yellow`%s`white`\r\n", (char *)lua_tostring(L, -1));
	return 0;
}

int lua_printGreen(lua_State *L)
{
	md_printf("`bright green`%s`white`\r\n", (char *)lua_tostring(L, -1));
	return 0;
}

void lua_push_cfunctions(lua_State *L);

void do_lua_script(char *script)
{
	lua_State *L;
	char buffer[PATH_MAX];

	if (script == NULL)
	{
		return;
	}

	snprintf(buffer, PATH_MAX, "%s.lua", script);

	L = luaL_newstate();
	luaL_openlibs(L);
	lua_push_cfunctions(L);
	if (luaL_dofile(L, buffer))
	{
		md_printf("`bright red`%s`white`\r\n", lua_tostring(L, -1));
	}
	lua_close(L);
}

int select_bbs(int type)
{
	int i;
	char buffer[8];
	int lines;

	while (1)
	{

		if (type == 1)
		{
			md_printf("\r\nWhich Galaxy do you want to attack...\r\n");
		}
		else
		{
			md_printf("\r\nWhich Galaxy do you want to send a message to...\r\n");
		}
		lines = 0;
		for (i = 0; i < InterBBSInfo.otherNodeCount; i++)
		{
			md_printf(" (%d) %s\r\n", i + 1, InterBBSInfo.otherNodes[i]->name);
			lines++;
			if (lines == 22) {
				md_printf("`bright white`Press any key to continue...`white`");
				md_getc();
				md_printf("\r\n");
				lines = 0;
			}
		}
		md_printf(" (0) Cancel\r\n");
		md_getstring(buffer, 8, '0', '9');
		i = atoi(buffer);
		if (i <= 0)
		{
			return 256;
		}

		if (InterBBSInfo.otherNodes[i - 1]->nodeNumber == InterBBSInfo.myNode->nodeNumber)
		{
			if (type == 1)
			{
				md_printf(" Cannot launch an armada against a member of your own galaxy!\r\n");
			}
			else
			{
				md_printf(" Cannot launch an inter galactic message to a member of your own galaxy!\r\n");
			}
			return 256;
		}
		if (i <= InterBBSInfo.otherNodeCount)
		{
			if (type == 1)
			{
				md_printf(" Sending armada to %s!\r\n", InterBBSInfo.otherNodes[i - 1]->name);
			}
			else
			{
				md_printf(" Sending a message to %s!\r\n", InterBBSInfo.otherNodes[i - 1]->name);
			}
			return InterBBSInfo.otherNodes[i - 1]->nodeNumber;
		}
	}
}

int is_highest_score_player()
{
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];
	uint64_t curscore = calculate_score(gPlayer);

	player_t *player;

    if (!open_users_database(&db)) {
		exit(1);
	}
	snprintf(sqlbuffer, 256, "SELECT gamename FROM users;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
	while (sqlite3_step(stmt) == SQLITE_ROW)
	{
		player = load_player_gn((const char *)sqlite3_column_text(stmt, 0));
		if (calculate_score(player) > curscore)
		{
			free(player);
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			return 0;
		}
		free(player);
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return 1;
}

int is_highest_score_player_ibbs()
{
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];
	uint64_t curscore;
	if (!open_interbbs_database(&db))
	{
		// Error opening the database
		dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	snprintf(sqlbuffer, 256, "SELECT gamename FROM scores WHERE score > ?;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
	curscore = calculate_score(gPlayer);

	sqlite3_bind_int(stmt, 1, curscore);
	rc = sqlite3_step(stmt);
	if (rc == SQLITE_ROW)
	{
		sqlite3_finalize(stmt);
		sqlite3_close(db);
		return 0;
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);
	return 1;
}

int select_ibbs_player(int addr, char *player_name)
{
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char buffer[8];
	char sqlbuffer[256];
	int count;
	char **names;
	int i;
	int lines;

    if (!open_interbbs_database(&db))
	{
		// Error opening the database
		dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	count = 0;
	names = NULL;

	snprintf(sqlbuffer, 256, "SELECT gamename FROM scores WHERE address = ?;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, addr);
	rc = sqlite3_step(stmt);
	while (rc == SQLITE_ROW)
	{
		if (names == NULL)
		{
			names = (char **)malloc(sizeof(char *));
			if (names == NULL)
			{
				sqlite3_finalize(stmt);
				sqlite3_close(db);
				return -1;
			}
		}
		else
		{
			names = (char **)realloc(names, sizeof(char *) * (count + 1));
			if (names == NULL)
			{
				sqlite3_finalize(stmt);
				sqlite3_close(db);
				return -1;
			}
		}

		names[count] = (char *)malloc(sizeof(char) * 17);
		if (names[count] == NULL)
		{
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			return -1;
		}
		strncpy(names[count], (const char *)sqlite3_column_text(stmt, 0), 17);

		count++;
		rc = sqlite3_step(stmt);
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	while (1)
	{
		lines = 0;
		md_printf("\r\nSelect an empire...\r\n");
		for (i = 0; i < count; i++)
		{
			md_printf(" (%d) %s\r\n", i + 1, names[i]);
			lines++;
			if (lines == 22) {
				md_printf("`bright white`Press any key to continue...`white`");
				md_getc();
				md_printf("\r\n");
				lines = 0;
			}
		}
		md_printf(" (0) Cancel\r\n");
		md_getstring(buffer, 8, '0', '9');
		i = atoi(buffer);
		if (i == 0)
		{
			for (i = 0; i < count; i++)
			{
				free(names[i]);
			}
			free(names);
			return -1;
		}
		if (i <= count)
		{
			strncpy(player_name, names[i - 1], 17);
			for (i = 0; i < count; i++)
			{
				free(names[i]);
			}
			free(names);
			return 0;
		}
	}
}

void send_system_message(char *body)
{
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	player_t *otherplayer;
	char sqlbuffer[256];

	if (!open_users_database(&db))
	{
		// Error opening the database
		dolog("Error opening user database: %s", sqlite3_errmsg(db));
		exit(1);
	}

	snprintf(sqlbuffer, 256, "SELECT gamename FROM USERS");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);

	rc = sqlite3_step(stmt);
	while (rc == SQLITE_ROW)
	{
		otherplayer = load_player_gn((const char *)sqlite3_column_text(stmt, 0));

		send_message(otherplayer, NULL, body);
		free(otherplayer);

		rc = sqlite3_step(stmt);
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);
}

void send_message(player_t *to, player_t *from, char *body)
{
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];
    char sysfrom[] = "SYSTEM";

    if (!open_msgs_database(&db))
	{
		// Error opening the database
		dolog("Error opening messages database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	if (from == NULL)
	{
		snprintf(sqlbuffer, 256, "INSERT INTO messages (recipient, 'from', system, date, seen, body) VALUES(?, ?, ?, ?, ?, ?)");
		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
		sqlite3_bind_text(stmt, 1, to->gamename, strlen(to->gamename), SQLITE_STATIC);
        sqlite3_bind_text(stmt, 2, sysfrom, strlen(sysfrom), SQLITE_STATIC);
		sqlite3_bind_int(stmt, 3, 1);
		sqlite3_bind_int(stmt, 4, time(NULL));
		sqlite3_bind_int(stmt, 5, 0);
		sqlite3_bind_text(stmt, 6, body, strlen(body), SQLITE_STATIC);
	}
	else
	{
		snprintf(sqlbuffer, 256, "INSERT INTO messages (recipient, 'from', system, date, seen, body) VALUES(?, ?, ?, ?, ?, ?)");
		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
		sqlite3_bind_text(stmt, 1, to->gamename, strlen(to->gamename), SQLITE_STATIC);
		sqlite3_bind_text(stmt, 2, from->gamename, strlen(from->gamename), SQLITE_STATIC);
		sqlite3_bind_int(stmt, 3, 0);
		sqlite3_bind_int(stmt, 4, time(NULL));
		sqlite3_bind_int(stmt, 5, 0);
		sqlite3_bind_text(stmt, 6, body, strlen(body), SQLITE_STATIC);
	}
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE)
	{
		// Error opening the database
		md_printf("Error saving to messages database: %s\r\n", sqlite3_errmsg(db));
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);
}

void unseen_msgs(player_t *player)
{
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];
	message_t **msg;
	int msg_count;
	int i;
	struct tm *ptr;

	if (!open_msgs_database(&db))
	{
		// Error opening the database
		dolog("Error opening user database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	snprintf(sqlbuffer, 256, "SELECT id, recipient, 'from', system, date, seen, body FROM messages WHERE recipient LIKE ?;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
	sqlite3_bind_text(stmt, 1, player->gamename, strlen(player->gamename), SQLITE_STATIC);

	msg = NULL;
	msg_count = 0;

	rc = sqlite3_step(stmt);
	while (rc == SQLITE_ROW)
	{

		if (msg == NULL)
		{
			msg = (message_t **)malloc(sizeof(message_t *));
		}
		else
		{
			msg = (message_t **)realloc(msg, sizeof(message_t *) * (msg_count + 1));
		}

		msg[msg_count] = (message_t *)malloc(sizeof(message_t));
		msg[msg_count]->id = sqlite3_column_int(stmt, 0);
		strncpy(msg[msg_count]->to, (const char *)sqlite3_column_text(stmt, 1), 17);
		if ((const char *)sqlite3_column_text(stmt, 2) != NULL)
		{
			strncpy(msg[msg_count]->from, (const char *)sqlite3_column_text(stmt, 2), 17);
		}
		msg[msg_count]->system = sqlite3_column_int(stmt, 3);
		msg[msg_count]->date = sqlite3_column_int(stmt, 4);
		msg[msg_count]->seen = sqlite3_column_int(stmt, 5);
		strncpy(msg[msg_count]->body, (const char *)sqlite3_column_text(stmt, 6), 256);
		msg_count++;

		rc = sqlite3_step(stmt);
	}

	if (rc != SQLITE_DONE)
	{
		dolog("Error: %s", sqlite3_errmsg(db));
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	// display unseen messages
	md_printf("\r\nDisplaying sub-space messages for %s\r\n", player->gamename);

	for (i = 0; i < msg_count; i++)
	{
		if (msg[i]->seen == 0)
		{
			if (msg[i]->system == 1)
			{
				ptr = localtime(&msg[i]->date);
				md_printf("`bright red`System `white`message sent on `bright cyan`%.2d/%.2d/%4d`white`\r\n", ptr->tm_mday, ptr->tm_mon + 1, ptr->tm_year + 1900);
				md_printf("`white`   %s`white`\r\n", msg[i]->body);
				msg[i]->seen = 1;
			}
			else
			{
				ptr = localtime(&msg[i]->date);
				md_printf("`white`Message sent from `bright green`%s`white` on `bright cyan`%.2d/%.2d/%4d`white`\r\n", msg[i]->from, ptr->tm_mday, ptr->tm_mon + 1, ptr->tm_year + 1900);
				md_printf("`white`   %s`white`\r\n", msg[i]->body);
				msg[i]->seen = 1;
			}
		}
	}

	md_printf("\r\nPress a key to continue\r\n");
	md_getc();

	// update messages
	if (!open_msgs_database(&db))
	{
		// Error opening the database
		dolog("Error opening user database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	for (i = 0; i < msg_count; i++)
	{
		snprintf(sqlbuffer, 256, "UPDATE messages SET seen=? WHERE id = ?");
		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
		sqlite3_bind_int(stmt, 1, msg[i]->seen);
		sqlite3_bind_int(stmt, 2, msg[i]->id);
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);
	}

	sqlite3_close(db);

	for (i = 0; i < msg_count; i++)
	{
		free(msg[i]);
	}
	free(msg);
}

void unseen_ibbs_msgs(player_t *player)
{
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];
	message_t **msg;
	int msg_count;
	int i;
	struct tm *ptr;
	char *systemname = NULL;
	int j;

	if (!open_interbbs_database(&db))
	{
		// Error opening the database
		sqlite3_close(db);
		exit(1);
	}
	snprintf(sqlbuffer, 256, "SELECT * FROM messages WHERE recipient LIKE ?;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
	sqlite3_bind_text(stmt, 1, player->gamename, strlen(player->gamename), SQLITE_STATIC);

	msg = NULL;
	msg_count = 0;

	rc = sqlite3_step(stmt);
	while (rc == SQLITE_ROW)
	{

		if (msg == NULL)
		{
			msg = (message_t **)malloc(sizeof(message_t *));
		}
		else
		{
			msg = (message_t **)realloc(msg, sizeof(message_t *) * (msg_count + 1));
		}

		msg[msg_count] = (message_t *)malloc(sizeof(message_t));
		msg[msg_count]->id = sqlite3_column_int(stmt, 0);
		strncpy(msg[msg_count]->to, (const char *)sqlite3_column_text(stmt, 1), 17);
		strncpy(msg[msg_count]->from, (const char *)sqlite3_column_text(stmt, 2), 17);
		msg[msg_count]->addr = sqlite3_column_int(stmt, 3);
		msg[msg_count]->date = sqlite3_column_int(stmt, 4);
		msg[msg_count]->seen = sqlite3_column_int(stmt, 5);
		strncpy(msg[msg_count]->body, (const char *)sqlite3_column_text(stmt, 6), 256);
		msg_count++;

		rc = sqlite3_step(stmt);
	}

	if (rc != SQLITE_DONE)
	{
		md_printf("Error: %s", sqlite3_errmsg(db));
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	// display unseen messages
	md_printf("\r\nDisplaying inter-galactic messages for %s\r\n", player->gamename);

	for (i = 0; i < msg_count; i++)
	{
		if (msg[i]->seen == 0)
		{
			ptr = localtime(&msg[i]->date);

			for (j = 0; j < InterBBSInfo.otherNodeCount; j++)
			{
				if (InterBBSInfo.otherNodes[j]->nodeNumber == msg[i]->addr)
				{
					systemname = InterBBSInfo.otherNodes[j]->name;
					break;
				}
			}

			md_printf("`white`Message sent from `bright yellow`%s`white` by `bright green`%s`white` on `bright cyan`%.2d/%.2d/%4d`white`\r\n", systemname, msg[i]->from, ptr->tm_mday, ptr->tm_mon + 1, ptr->tm_year + 1900);
			md_printf("`white`   %s`white`\r\n", msg[i]->body);
			msg[i]->seen = 1;
		}
	}

	md_printf("\r\nPress a key to continue\r\n");
	md_getc();

	// update messages
    if (!open_interbbs_database(&db))
	{
		// Error opening the database
		dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	for (i = 0; i < msg_count; i++)
	{
		snprintf(sqlbuffer, 256, "UPDATE messages SET seen=? WHERE id = ?");
		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
		sqlite3_bind_int(stmt, 1, msg[i]->seen);
		sqlite3_bind_int(stmt, 2, msg[i]->id);
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);
	}

	sqlite3_close(db);

	for (i = 0; i < msg_count; i++)
	{
		free(msg[i]);
	}
	free(msg);
}

uint64_t calculate_score(player_t *player)
{
	uint64_t score;
	uint64_t planet_count = (player->planets_ore + player->planets_food + player->planets_industrial + player->planets_military);

	score = 0;
	score += player->troops / 5;
	score += player->generals;
	score += player->fighters * 2;
	score += player->defence_stations * 2;
	score += player->command_ship * 60;
	
	if (planet_count <= 20) {
		score += planet_count * 20;
	} else {
		score += 400 + planet_count;
	}
	score += player->food / 10000;
	score += player->population / 1000;
	score += (player->sprockets / 10000);
	score += (player->ore / 10000);
	score /= 100;

	return score;
}

player_t *load_player_gn(const char *gamename)
{
	player_t *thePlayer;
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];

	if (!open_users_database(&db))
	{
		// Error opening the database
		dolog("Error opening user database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	snprintf(sqlbuffer, 256, "SELECT * FROM users WHERE gamename LIKE ?;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
	sqlite3_bind_text(stmt, 1, gamename, strlen(gamename), SQLITE_STATIC);

	rc = sqlite3_step(stmt);
	if (rc == SQLITE_ROW)
	{
		thePlayer = (player_t *)malloc(sizeof(player_t));

		if (!thePlayer)
		{
			dolog("OOM");
			md_exit(-1);
			exit(-1);
		}

		thePlayer->id = sqlite3_column_int(stmt, 0);
		strncpy(thePlayer->gamename, (const char *)sqlite3_column_text(stmt, 2), 17);

		thePlayer->troops = sqlite3_column_int(stmt, 3);
		thePlayer->generals = sqlite3_column_int(stmt, 4);
		thePlayer->fighters = sqlite3_column_int(stmt, 5);
		thePlayer->defence_stations = sqlite3_column_int(stmt, 6);

		thePlayer->population = sqlite3_column_int(stmt, 7);
		thePlayer->food = sqlite3_column_int(stmt, 8);
		thePlayer->credits = sqlite3_column_int64(stmt, 9);

		thePlayer->planets_food = sqlite3_column_int64(stmt, 10);
		thePlayer->planets_ore = sqlite3_column_int64(stmt, 11);
		thePlayer->planets_industrial = sqlite3_column_int64(stmt, 12);
		thePlayer->planets_military = sqlite3_column_int64(stmt, 13);
		thePlayer->planets_urban = sqlite3_column_int64(stmt, 14);
		thePlayer->command_ship = sqlite3_column_int(stmt, 15);

		thePlayer->turns_left = sqlite3_column_int(stmt, 16);
		thePlayer->last_played = sqlite3_column_int(stmt, 17);
		thePlayer->spies = sqlite3_column_int(stmt, 18);
		thePlayer->last_score = sqlite3_column_int64(stmt, 19);
		thePlayer->total_turns = sqlite3_column_int(stmt, 20);
		thePlayer->bank_balance = sqlite3_column_int64(stmt, 21);
		thePlayer->sprockets = sqlite3_column_int(stmt, 22);
		thePlayer->ore = sqlite3_column_int(stmt, 23);
		sqlite3_finalize(stmt);
		sqlite3_close(db);
	}
	else
	{
		sqlite3_finalize(stmt);
		sqlite3_close(db);
		thePlayer = NULL;
	}
	return thePlayer;
}

player_t *load_player(char *bbsname)
{
	player_t *thePlayer;
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];

	if (!open_users_database(&db))
	{
		// Error opening the database
		dolog("Error opening user database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	snprintf(sqlbuffer, 256, "SELECT * FROM users WHERE bbsname LIKE ?;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
	sqlite3_bind_text(stmt, 1, bbsname, strlen(bbsname), SQLITE_STATIC);

	rc = sqlite3_step(stmt);
	if (rc == SQLITE_ROW)
	{
		thePlayer = (player_t *)malloc(sizeof(player_t));

		if (!thePlayer)
		{
			dolog("OOM");
			md_exit(-1);
			exit(-1);
		}

		thePlayer->id = sqlite3_column_int(stmt, 0);
		strncpy(thePlayer->gamename, (const char *)sqlite3_column_text(stmt, 2), 17);

		thePlayer->troops = sqlite3_column_int(stmt, 3);
		thePlayer->generals = sqlite3_column_int(stmt, 4);
		thePlayer->fighters = sqlite3_column_int(stmt, 5);
		thePlayer->defence_stations = sqlite3_column_int(stmt, 6);

		thePlayer->population = sqlite3_column_int(stmt, 7);
		thePlayer->food = sqlite3_column_int(stmt, 8);
		thePlayer->credits = sqlite3_column_int64(stmt, 9);

		thePlayer->planets_food = sqlite3_column_int64(stmt, 10);
		thePlayer->planets_ore = sqlite3_column_int64(stmt, 11);
		thePlayer->planets_industrial = sqlite3_column_int64(stmt, 12);
		thePlayer->planets_military = sqlite3_column_int64(stmt, 13);
		thePlayer->planets_urban = sqlite3_column_int64(stmt, 14);
		thePlayer->command_ship = sqlite3_column_int(stmt, 15);

		thePlayer->turns_left = sqlite3_column_int(stmt, 16);
		thePlayer->last_played = sqlite3_column_int(stmt, 17);
		thePlayer->spies = sqlite3_column_int(stmt, 18);
		thePlayer->last_score = sqlite3_column_int64(stmt, 19);
		thePlayer->total_turns = sqlite3_column_int(stmt, 20);
		thePlayer->bank_balance = sqlite3_column_int64(stmt, 21);
		thePlayer->sprockets = sqlite3_column_int(stmt, 22);
		thePlayer->ore = sqlite3_column_int(stmt, 23);
		sqlite3_finalize(stmt);
		sqlite3_close(db);
	}
	else
	{
		sqlite3_finalize(stmt);
		sqlite3_close(db);
		thePlayer = NULL;
	}
	return thePlayer;
}

void build_interbbs_scorefile()
{

	FILE *fptr, *fptr2;
	sqlite3 *db;
	char sqlbuffer[256];
	int rc;
	sqlite3_stmt *stmt;
	char c;
	ibbsscores_t **scores;
	int player_count;
	ibbsscores_t *ptr;
	int i;
	int j;
	int gotpipe = 0;
	char *winner;
	int winner_system;

	winner = NULL;

	fptr = fopen("ibbs_winner.dat", "r");
	if (fptr)
	{
		fgets(sqlbuffer, 256, fptr);
		sqlbuffer[strlen(sqlbuffer) - 1] = '\0';
		winner = strdup(sqlbuffer);
		fgets(sqlbuffer, 256, fptr);
		winner_system = atoi(sqlbuffer);
		fclose(fptr);
	}
	scores = NULL;
	player_count = 0;

	if (!open_users_database(&db))
	{
		// Error opening the database
		dolog("Error opening users database: %s\n", sqlite3_errmsg(db));
		exit(1);
	}

	snprintf(sqlbuffer, 256, "SELECT gamename,last_score FROM users;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);

	rc = sqlite3_step(stmt);
	while (rc == SQLITE_ROW)
	{
		if (scores == NULL)
		{
			scores = (ibbsscores_t **)malloc(sizeof(ibbsscores_t *));
		}
		else
		{
			scores = (ibbsscores_t **)realloc(scores, sizeof(ibbsscores_t *) * (player_count + 1));
		}

		if (scores == NULL)
		{
			dolog("OOM");
			exit(-1);
		}

		scores[player_count] = (ibbsscores_t *)malloc(sizeof(ibbsscores_t));
		strncpy(scores[player_count]->player_name, (char *)sqlite3_column_text(stmt, 0), 17);
		strcpy(scores[player_count]->bbs_name, InterBBSInfo.myNode->name);
		scores[player_count]->score = sqlite3_column_int(stmt, 1);

		scores[player_count]->winner = 0;

		if (winner != NULL)
		{
			if (winner_system == InterBBSInfo.myNode->nodeNumber && strcmp(scores[player_count]->player_name, winner) == 0)
			{
				scores[player_count]->winner = 1;
			}
		}
		player_count++;
		rc = sqlite3_step(stmt);
	}

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	if (!open_interbbs_database(&db))
	{
		// Error opening the database
		dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
		exit(1);
	}

	snprintf(sqlbuffer, 256, "SELECT gamename,address,score FROM scores;");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
	while (sqlite3_step(stmt) == SQLITE_ROW)
	{



		for (i = 0; i < InterBBSInfo.otherNodeCount; i++)
		{
			if (sqlite3_column_int(stmt, 1) == InterBBSInfo.otherNodes[i]->nodeNumber)
			{
				if (scores == NULL)
				{
					scores = (ibbsscores_t **)malloc(sizeof(ibbsscores_t *));
				}
				else
				{
					scores = (ibbsscores_t **)realloc(scores, sizeof(ibbsscores_t *) * (player_count + 1));
				}
		
				if (scores == NULL)
				{
					dolog("OOM");
					exit(-1);
				}
		
				scores[player_count] = (ibbsscores_t *)malloc(sizeof(ibbsscores_t));
				strncpy(scores[player_count]->player_name, (char *)sqlite3_column_text(stmt, 0), 17);			
			
				strncpy(scores[player_count]->bbs_name, InterBBSInfo.otherNodes[i]->name, 40);
				if (winner != NULL)
				{
					if (InterBBSInfo.otherNodes[i]->nodeNumber == winner_system && strcmp(winner, scores[player_count]->player_name) == 0)
					{
						scores[player_count]->winner = 1;
					}
				}
						
				scores[player_count]->score = sqlite3_column_int(stmt, 2);
				scores[player_count]->winner = 0;
				player_count++;
				break;
			}
		}
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);
	if (winner != NULL)
	{
		free(winner);
	}
	for (i = 0; i < player_count - 1; i++)
	{
		for (j = 0; j < player_count - i - 1; j++)
		{
			if (scores[j]->score < scores[j + 1]->score)
			{
				ptr = scores[j];
				scores[j] = scores[j + 1];
				scores[j + 1] = ptr;
			}
		}
	}
	gotpipe = 0;
	fptr = fopen("ibbs_scores.ans", "w");

	if (fptr)
	{
		fptr2 = fopen("ibbs_score_header.ans", "r");
		if (fptr2)
		{
			c = fgetc(fptr2);
			while (!feof(fptr2))
			{
				if (gotpipe == 1)
				{
					if (c == 'V')
					{
						fprintf(fptr, "v%d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
					}
					else
					{
						fprintf(fptr, "|%c", c);
					}
					gotpipe = 0;
				}
				else
				{
					if (c == '|')
					{
						gotpipe = 1;
					}
					else
					{
						fputc(c, fptr);
					}
				}
				c = fgetc(fptr2);
			}
			fclose(fptr2);
		}
		for (i = 0; i < player_count; i++)
		{
#if defined(_MSC_VER) || defined(WIN32)
			if (scores[i]->winner)
			{
				fprintf(fptr, "\x1b[0m %-31.31s %-31.31s %11" PRIu64 " W\n", scores[i]->player_name, scores[i]->bbs_name, scores[i]->score);
			}
			else
			{
				fprintf(fptr, "\x1b[0m %-31.31s %-31.31s %11" PRIu64 "\n", scores[i]->player_name, scores[i]->bbs_name, scores[i]->score);
			}
#else
			if (scores[i]->winner)
			{
				fprintf(fptr, "\x1b[0m %-31.31s %-31.31s %11" PRIu64 " W\r\n", scores[i]->player_name, scores[i]->bbs_name, scores[i]->score);
			}
			else
			{
				fprintf(fptr, "\x1b[0m %-31.31s %-31.31s %11" PRIu64 "\r\n", scores[i]->player_name, scores[i]->bbs_name, scores[i]->score);
			}
#endif
		}
		gotpipe = 0;
		fptr2 = fopen("ibbs_score_footer.ans", "r");
		if (fptr2)
		{
			c = fgetc(fptr2);
			while (!feof(fptr2))
			{
				if (gotpipe == 1)
				{
					if (c == 'V')
					{
						fprintf(fptr, "v%d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
					}
					else
					{
						fprintf(fptr, "|%c", c);
					}
					gotpipe = 0;
				}
				else
				{
					if (c == '|')
					{
						gotpipe = 1;
					}
					else
					{
						fputc(c, fptr);
					}
				}
				c = fgetc(fptr2);
			}
			fclose(fptr2);
		}
		fclose(fptr);
	}
	gotpipe = 0;
	fptr = fopen("ibbs_scores.asc", "w");

	if (fptr)
	{
		fptr2 = fopen("ibbs_score_header.asc", "r");
		if (fptr2)
		{
			c = fgetc(fptr2);
			while (!feof(fptr2))
			{
				if (gotpipe == 1)
				{
					if (c == 'V')
					{
						fprintf(fptr, "v%d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
					}
					else
					{
						fprintf(fptr, "|%c", c);
					}
					gotpipe = 0;
				}
				else
				{
					if (c == '|')
					{
						gotpipe = 1;
					}
					else
					{
						fputc(c, fptr);
					}
				}
				c = fgetc(fptr2);
			}
			fclose(fptr2);
		}
		for (i = 0; i < player_count; i++)
		{
#if defined(_MSC_VER) || defined(WIN32)
			if (scores[i]->winner)
			{
				fprintf(fptr, " %-31.31s %-31.31s %11" PRIu64 " W\n", scores[i]->player_name, scores[i]->bbs_name, scores[i]->score);
			}
			else
			{
				fprintf(fptr, " %-31.31s %-31.31s %11" PRIu64 "\n", scores[i]->player_name, scores[i]->bbs_name, scores[i]->score);
			}
#else
			if (scores[i]->winner)
			{
				fprintf(fptr, " %-31.31s %-31.31s %11" PRIu64 " W\r\n", scores[i]->player_name, scores[i]->bbs_name, scores[i]->score);
			}
			else
			{
				fprintf(fptr, " %-31.31s %-31.31s %11" PRIu64 "\r\n", scores[i]->player_name, scores[i]->bbs_name, scores[i]->score);
			}
#endif
		}
		gotpipe = 0;
		fptr2 = fopen("ibbs_score_footer.asc", "r");
		if (fptr2)
		{
			c = fgetc(fptr2);
			while (!feof(fptr2))
			{
				if (gotpipe == 1)
				{
					if (c == 'V')
					{
						fprintf(fptr, "v%d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
					}
					else
					{
						fprintf(fptr, "|%c", c);
					}
					gotpipe = 0;
				}
				else
				{
					if (c == '|')
					{
						gotpipe = 1;
					}
					else
					{
						fputc(c, fptr);
					}
				}
				c = fgetc(fptr2);
			}
			fclose(fptr2);
		}
		fclose(fptr);
	}

	for (i = 0; i < player_count; i++)
	{
		free(scores[i]);
	}
	free(scores);
}

void build_scorefile()
{
	FILE *fptr, *fptr2;
	sqlite3 *db;
	char sqlbuffer[256];
	sqlite3_stmt *stmt;
	char c;
	int gotpipe = 0;
	player_t *player;
	char *winner;

	winner = NULL;
	gotpipe = 0;

	fptr = fopen("ibbs_winner.dat", "r");

	if (fptr)
	{
		fgets(sqlbuffer, 256, fptr);

		winner = strdup(sqlbuffer);

		fclose(fptr);
	}

	fptr = fopen("scores.ans", "w");

	if (fptr)
	{
		fptr2 = fopen("score_header.ans", "r");
		if (fptr2)
		{
			c = fgetc(fptr2);
			while (!feof(fptr2))
			{
				if (gotpipe == 1)
				{
					if (c == 'V')
					{
						fprintf(fptr, "v%d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
					}
					else
					{
						fprintf(fptr, "|%c", c);
					}
					gotpipe = 0;
				}
				else
				{
					if (c == '|')
					{
						gotpipe = 1;
					}
					else
					{
						fputc(c, fptr);
					}
				}
				c = fgetc(fptr2);
			}
			fclose(fptr2);
		}

		if (!open_users_database(&db))
		{
			// Error opening the database
			dolog("Error opening users database: %s", sqlite3_errmsg(db));
			exit(1);
		}
		snprintf(sqlbuffer, 256, "SELECT gamename FROM users;");
		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
		while (sqlite3_step(stmt) == SQLITE_ROW)
		{
			player = load_player_gn((char *)sqlite3_column_text(stmt, 0));
#if defined(_MSC_VER) || defined(WIN32)
			if (winner != NULL && strcmp(player->gamename, winner) == 0)
			{
				fprintf(fptr, "\x1b[0m %-64.64s %11" PRIu64 " W\n", player->gamename, calculate_score(player));
			}
			else
			{
				fprintf(fptr, "\x1b[0m %-64.64s %11" PRIu64 "\n", player->gamename, calculate_score(player));
			}
#else
			if (winner != NULL && strcmp(player->gamename, winner) == 0)
			{
				fprintf(fptr, "\x1b[0m %-64.64s %11" PRIu64 " W\r\n", player->gamename, calculate_score(player));
			}
			else
			{
				fprintf(fptr, "\x1b[0m %-64.64s %11" PRIu64 "\r\n", player->gamename, calculate_score(player));
			}
#endif
			free(player);
		}

		sqlite3_finalize(stmt);
		sqlite3_close(db);
		gotpipe = 0;
		fptr2 = fopen("score_footer.ans", "r");
		if (fptr2)
		{
			c = fgetc(fptr2);
			while (!feof(fptr2))
			{
				if (gotpipe == 1)
				{
					if (c == 'V')
					{
						fprintf(fptr, "v%d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
					}
					else
					{
						fprintf(fptr, "|%c", c);
					}
					gotpipe = 0;
				}
				else
				{
					if (c == '|')
					{
						gotpipe = 1;
					}
					else
					{
						fputc(c, fptr);
					}
				}
				c = fgetc(fptr2);
			}
			fclose(fptr2);
		}

		fclose(fptr);
	}
	gotpipe = 0;

	fptr = fopen("scores.asc", "w");

	if (fptr)
	{
		fptr2 = fopen("score_header.asc", "r");
		if (fptr2)
		{
			c = fgetc(fptr2);
			while (!feof(fptr2))
			{
				if (gotpipe == 1)
				{
					if (c == 'V')
					{
						fprintf(fptr, "v%d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
					}
					else
					{
						fprintf(fptr, "|%c", c);
					}
					gotpipe = 0;
				}
				else
				{
					if (c == '|')
					{
						gotpipe = 1;
					}
					else
					{
						fputc(c, fptr);
					}
				}
				c = fgetc(fptr2);
			}
			fclose(fptr2);
		}

		if (!open_users_database(&db))
		{
			// Error opening the database
			dolog("Error opening users database: %s", sqlite3_errmsg(db));
			exit(1);
		}
		snprintf(sqlbuffer, 256, "SELECT gamename FROM users;");
		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
		while (sqlite3_step(stmt) == SQLITE_ROW)
		{
			player = load_player_gn((char *)sqlite3_column_text(stmt, 0));
#if defined(_MSC_VER) || defined(WIN32)
			if (winner != NULL && strcmp(player->gamename, winner) == 0)
			{
				fprintf(fptr, "\x1b[0m %-64.64s %11" PRIu64 " W\n", player->gamename, calculate_score(player));
			}
			else
			{
				fprintf(fptr, "\x1b[0m %-64.64s %11" PRIu64 "\n", player->gamename, calculate_score(player));
			}
#else
			if (winner != NULL && strcmp(player->gamename, winner) == 0)
			{
				fprintf(fptr, "\x1b[0m %-64.64s %11" PRIu64 " W\r\n", player->gamename, calculate_score(player));
			}
			else
			{
				fprintf(fptr, "\x1b[0m %-64.64s %11" PRIu64 "\r\n", player->gamename, calculate_score(player));
			}
#endif
			free(player);
		}

		sqlite3_finalize(stmt);
		sqlite3_close(db);
		gotpipe = 0;
		fptr2 = fopen("score_footer.asc", "r");
		if (fptr2)
		{
			c = fgetc(fptr2);
			while (!feof(fptr2))
			{
				if (gotpipe == 1)
				{
					if (c == 'V')
					{
						fprintf(fptr, "v%d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
					}
					else
					{
						fprintf(fptr, "|%c", c);
					}
					gotpipe = 0;
				}
				else
				{
					if (c == '|')
					{
						gotpipe = 1;
					}
					else
					{
						fputc(c, fptr);
					}
				}
				c = fgetc(fptr2);
			}
			fclose(fptr2);
		}

		fclose(fptr);
	}
	if (winner != NULL)
	{
		free(winner);
	}
}

player_t *new_player(char *bbsname)
{
	player_t *player;
	char c;
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[] = "SELECT * FROM users WHERE gamename LIKE ?;";
	player = (player_t *)malloc(sizeof(player_t));

	memset(player, 0, sizeof(player_t));

	if (!player)
	{
		dolog("OOM!");
		md_exit(-1);
		exit(-1);
	}

	player->id = -1;

	strncpy(player->bbsname, bbsname, 256);

	md_sendfile("instruction.ans", TRUE);

	while (1)
	{
		md_printf("\r\n`white`Welcome to `bright blue`Galactic Dynasty`white`!\r\n");
		md_printf("What would you like to name your empire: ");
		md_getstring(player->gamename, 17, 32, 126);

		if (strlen(player->gamename) == 0)
		{
			free(player);
			return NULL;
		}

		if (strlen(player->gamename) == 1)
		{
			md_printf("\r\nSorry, that name is too short.\r\n");
			continue;
		}


		if (!open_users_database(&db))
		{
			// Error opening the database
			dolog("Error opening user database: %s", sqlite3_errmsg(db));
			md_exit(1);
		}

		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
		sqlite3_bind_text(stmt, 1, player->gamename, strlen(player->gamename), SQLITE_STATIC);

		rc = sqlite3_step(stmt);
		if (rc == SQLITE_ROW)
		{
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			md_printf("\r\n\r\nSorry, this name is taken.\r\n");
		}
		else
		{
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			md_printf("\r\n\r\nDoes the mighty empire of `bright green`%s`white` sound ok? (Y/N) ", player->gamename);
			c = md_get_answer("yYnN");
			if (tolower(c) == 'y')
			{
				break;
			}
		}
		
	}

	player->troops = 100;
	player->generals = 0;
	player->fighters = 0;
	player->defence_stations = 0;
	player->spies = 0;

	player->population = 250;
	player->food = 50;
	player->credits = 1000;

	player->planets_food = 3;
	player->planets_ore = 0;
	player->planets_industrial = 0;
	player->planets_military = 0;
	player->planets_urban = 6;
	player->command_ship = 0;

	player->turns_left = turns_per_day;
	player->last_played = time(NULL);
	player->last_score = 0;
	player->total_turns = 0;
	player->bank_balance = 0;
	player->sprockets = 0;
	player->ore = 0;

	return player;
}

void list_empires(player_t *me)
{
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];
	int lines;

	if (!open_users_database(&db))
	{
		// Error opening the database
		dolog("Error opening user database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	snprintf(sqlbuffer, 256, "SELECT gamename FROM USERS");
	sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);

	rc = sqlite3_step(stmt);
	lines = 0;
	while (rc == SQLITE_ROW)
	{
		if (strcmp((const char *)sqlite3_column_text(stmt, 0), me->gamename) != 0)
		{
			md_printf("%s\r\n", sqlite3_column_text(stmt, 0));
			lines++;
			if (lines == 22) {
				md_printf("`bright white`Press any key to continue...`white`");
				md_getc();
				md_printf("\r\n");
				lines = 0;
			}
		}
		rc = sqlite3_step(stmt);
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);
}

void save_player(player_t *player)
{
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[1024];

	if (!open_users_database(&db))
	{
		// Error opening the database
		dolog("Error opening user database: %s", sqlite3_errmsg(db));
		exit(1);
	}
	if (player->id == -1)
	{
		snprintf(sqlbuffer, 1024, "INSERT INTO users (bbsname, gamename, troops, generals, fighters, defence_stations, "
								  "population, food, credits, planets_food, planets_ore, planets_industrial, "
								  "planets_military, command_ship, turns_left, last_played, spies, last_score, total_turns, planets_urban, bank_balance, sprockets, ore) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
		sqlite3_bind_text(stmt, 1, player->bbsname, strlen(player->bbsname), SQLITE_STATIC);
		sqlite3_bind_text(stmt, 2, player->gamename, strlen(player->gamename), SQLITE_STATIC);
		sqlite3_bind_int(stmt, 3, player->troops);
		sqlite3_bind_int(stmt, 4, player->generals);
		sqlite3_bind_int(stmt, 5, player->fighters);
		sqlite3_bind_int(stmt, 6, player->defence_stations);
		sqlite3_bind_int(stmt, 7, player->population);
		sqlite3_bind_int(stmt, 8, player->food);
		sqlite3_bind_int64(stmt, 9, player->credits);
		sqlite3_bind_int64(stmt, 10, player->planets_food);
		sqlite3_bind_int64(stmt, 11, player->planets_ore);
		sqlite3_bind_int64(stmt, 12, player->planets_industrial);
		sqlite3_bind_int64(stmt, 13, player->planets_military);
		sqlite3_bind_int(stmt, 14, player->command_ship);
		sqlite3_bind_int(stmt, 15, player->turns_left);
		sqlite3_bind_int(stmt, 16, player->last_played);
		sqlite3_bind_int(stmt, 17, player->spies);
		sqlite3_bind_int64(stmt, 18, player->last_score);
		sqlite3_bind_int(stmt, 19, player->total_turns);
		sqlite3_bind_int64(stmt, 20, player->planets_urban);
		sqlite3_bind_int64(stmt, 21, player->bank_balance);
		sqlite3_bind_int(stmt, 22, player->sprockets);
		sqlite3_bind_int(stmt, 23, player->ore);
	}
	else
	{
		snprintf(sqlbuffer, 1024, "UPDATE users SET gamename=?,"
								  "troops=?,"
								  "generals=?,"
								  "fighters=?,"
								  "defence_stations=?,"
								  "population=?,"
								  "food=?,"
								  "credits=?,"
								  "planets_food=?, "
								  "planets_ore=?, "
								  "planets_industrial=?, "
								  "planets_military=?,"
								  "command_ship=?,"
								  "turns_left=?,"
								  "last_played=?,"
								  "spies=?, "
								  "last_score=?, "
								  "total_turns=?, planets_urban=?, bank_balance = ?, sprockets = ?, ore = ? WHERE id=?;");
		sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
		sqlite3_bind_text(stmt, 1, player->gamename, strlen(player->gamename), SQLITE_STATIC);
		sqlite3_bind_int(stmt, 2, player->troops);
		sqlite3_bind_int(stmt, 3, player->generals);
		sqlite3_bind_int(stmt, 4, player->fighters);
		sqlite3_bind_int(stmt, 5, player->defence_stations);
		sqlite3_bind_int(stmt, 6, player->population);
		sqlite3_bind_int(stmt, 7, player->food);
		sqlite3_bind_int64(stmt, 8, player->credits);
		sqlite3_bind_int64(stmt, 9, player->planets_food);
		sqlite3_bind_int64(stmt, 10, player->planets_ore);
		sqlite3_bind_int64(stmt, 11, player->planets_industrial);
		sqlite3_bind_int64(stmt, 12, player->planets_military);
		sqlite3_bind_int(stmt, 13, player->command_ship);
		sqlite3_bind_int(stmt, 14, player->turns_left);
		sqlite3_bind_int(stmt, 15, player->last_played);
		sqlite3_bind_int(stmt, 16, player->spies);
		sqlite3_bind_int64(stmt, 17, player->last_score);
		sqlite3_bind_int(stmt, 18, player->total_turns);
		sqlite3_bind_int64(stmt, 19, player->planets_urban);
		sqlite3_bind_int64(stmt, 20, player->bank_balance);
		sqlite3_bind_int(stmt, 21, player->sprockets);
		sqlite3_bind_int(stmt, 22, player->ore);
		sqlite3_bind_int(stmt, 23, player->id);
	}
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE)
	{
		// Error opening the database
		md_printf("Error saving to users database: %s\r\n", sqlite3_errmsg(db));
		sqlite3_close(db);
		return;
	}
	sqlite3_finalize(stmt);
	sqlite3_close(db);

	build_scorefile();
}

int do_longrange_sabotage(char *victim, char *attacker, int from, int spies, ibbsmsg_t *msg)
{
	char bbs_name[40];
	int i, p;
	int caught;
	char message[256];
	uint32_t j;
	uint64_t planets;
	uint64_t planets_to_destroy;
	int done = 0;
	player_t *victim_player = load_player_gn(victim);
	if (victim_player == NULL)
	{
		return -1;
	}

	memset(bbs_name, 0, 40);

	for (i = 0; i < InterBBSInfo.otherNodeCount; i++)
	{
		if (from == InterBBSInfo.otherNodes[i]->nodeNumber)
		{
			strncpy(bbs_name, InterBBSInfo.otherNodes[i]->name, 40);
			break;
		}
	}

	if (strlen(bbs_name) == 0)
	{
		dolog("InterBBS Sabotage: Empire name mismatch");
		return -1;
	}

	msg->troops = spies;
	msg->fighters = 0;
	msg->generals = 0;

	for (i = 0; i < spies; i++)
	{
		caught = rand() % 100 + 1;

		if (caught > 50 || victim_player->total_turns <= turns_in_protection)
		{
			msg->fighters++;
		}
		else
		{
			planets = (victim_player->planets_food + victim_player->planets_urban + victim_player->planets_military + victim_player->planets_ore + victim_player->planets_industrial) / 10;
			if (planets > 0)
			{
				planets_to_destroy = rand() % planets + 1;
				for (j = 0; j < planets_to_destroy; j++)
				{
					msg->generals++;
					done = 0;
					while (!done)
					{
						p = rand() % 5;
						switch (p)
						{
						case 0:
							if (victim_player->planets_food > 0)
							{
								victim_player->planets_food--;
								done = 1;
							}
							break;
						case 1:
							if (victim_player->planets_urban > 0)
							{
								victim_player->planets_urban--;
								done = 1;
							}
							break;
						case 2:
							if (victim_player->planets_military > 0)
							{
								victim_player->planets_military--;
								done = 1;
							}
							break;
						case 3:
							if (victim_player->planets_ore > 0)
							{
								victim_player->planets_ore--;
								done = 1;
							}
							break;
						case 4:
							if (victim_player->planets_industrial > 0)
							{
								victim_player->planets_industrial--;
								done = 1;
							}
							break;
						default:
							dolog("Unknown planet type!");
							done = 1;
							break;
						}
					}
				}
			}
		}
	}

	save_player(victim_player);

	snprintf(message, 256, "Spies from %s (%s) sabotaged %u planets!!", attacker, bbs_name, msg->generals);
	send_message(victim_player, NULL, message);
	msg->type = 11;
	msg->from = InterBBSInfo.myNode->nodeNumber;
	strcpy(msg->player_name, attacker);
	strcpy(msg->victim_name, victim);

	msg->created = time(NULL);
	msg->plunder_credits = 0;
	msg->plunder_people = 0;
	msg->plunder_food = 0;
	msg->score = 0;
	free(victim_player);
	return 0;
}

int do_longrange_spy(char *victim, char *attacker, int from, ibbsmsg_t *msg)
{
	char bbs_name[40];
	int i;
	int caught;
	char message[256];
	player_t *victim_player = load_player_gn(victim);
	if (victim_player == NULL)
	{
		return -1;
	}

	memset(bbs_name, 0, 40);

	for (i = 0; i < InterBBSInfo.otherNodeCount; i++)
	{
		if (from == InterBBSInfo.otherNodes[i]->nodeNumber)
		{
			strncpy(bbs_name, InterBBSInfo.otherNodes[i]->name, 40);
			break;
		}
	}

	if (strlen(bbs_name) == 0)
	{
		dolog("InterBBS Spy: Empire name mismatch");
		return -1;
	}

	caught = rand() % 100 + 1;

	if (caught > 75 || victim_player->total_turns <= turns_in_protection)
	{
		// caught!
		snprintf(message, 256, "A spy from %s (%s) was caught trying to infiltrate your realm!", attacker, bbs_name);
		send_message(victim_player, NULL, message);
		msg->type = 10;
		msg->from = InterBBSInfo.myNode->nodeNumber;
		strcpy(msg->player_name, attacker);
		strcpy(msg->victim_name, victim);

		msg->created = time(NULL);
		msg->plunder_credits = 0;
		msg->plunder_people = 0;
		msg->plunder_food = 0;
		msg->troops = 0;
		msg->fighters = 0;
		msg->generals = 0;
		msg->score = 0;
		free(victim_player);
	}
	else
	{
		msg->type = 10;
		msg->from = InterBBSInfo.myNode->nodeNumber;
		strcpy(msg->player_name, attacker);
		strcpy(msg->victim_name, victim);

		msg->created = time(NULL);
		if (victim_player->bank_balance > 0)
		{
			msg->plunder_credits = victim_player->credits + victim_player->bank_balance;
		}
		else
		{
			msg->plunder_credits = victim_player->credits;
		}
		msg->plunder_people = victim_player->population;
		msg->plunder_food = victim_player->planets_food + victim_player->planets_industrial + victim_player->planets_military + victim_player->planets_ore + victim_player->planets_urban;
		msg->troops = victim_player->troops;
		msg->fighters = victim_player->defence_stations;
		msg->generals = victim_player->generals;
		msg->score = 1;
		free(victim_player);
	}
	return 0;
}

int do_interbbs_battle(char *victim, char *attacker, int from, uint32_t troops, uint32_t generals, uint32_t fighters, ibbsmsg_t *msg)
{
	uint32_t plunder_people;
	uint64_t plunder_credits;
	uint32_t plunder_food;
	uint32_t attack;
	uint32_t defence;
	double victory_chance;
	int battle;
	uint32_t enemy_troops;
	uint32_t enemy_generals;
	uint32_t enemy_defence_stations;
	char bbs_name[40];
	char message[256];
	int i;
	int dtroops;
	int dgenerals;
	int dfighters;
	int difference;

	player_t *victim_player = load_player_gn(victim);
	if (victim_player == NULL)
	{
		return -1;
	}

	memset(bbs_name, 0, 40);

	for (i = 0; i < InterBBSInfo.otherNodeCount; i++)
	{
		if (from == InterBBSInfo.otherNodes[i]->nodeNumber)
		{
			strncpy(bbs_name, InterBBSInfo.otherNodes[i]->name, 40);
			break;
		}
	}

	if (strlen(bbs_name) == 0)
	{
		dolog("InterBBS Battle: Empire name mismatch");
		return -1;
	}

	if (victim_player->total_turns <= turns_in_protection)
	{
		msg->type = 3;
		msg->from = InterBBSInfo.myNode->nodeNumber;
		strcpy(msg->player_name, attacker);
		strcpy(msg->victim_name, victim);

		msg->created = time(NULL);
		msg->plunder_credits = 0;
		msg->plunder_people = 0;
		msg->plunder_food = 0;
		msg->troops = troops;
		msg->fighters = fighters;
		msg->generals = generals;
		msg->score = 2;
		free(victim_player);
		return 0;
	}

	// attack soldiers
	attack = (3 * troops + 1 * fighters) + ((3 * troops + 1 * fighters) * (generals / 100));
	defence = 10 * victim_player->troops;

	// attack defence stations
	attack = attack + (1 * troops + 4 * fighters + ((1 * troops + 4 * fighters) * (generals / 100)));
	defence = defence + (25 * victim_player->defence_stations);

	victory_chance = ((double)attack / ((double)attack + (double)defence));
	battle = rand() % 100 + 1;

	if (battle < (victory_chance * 100))
	{
		// victory
		// people
		plunder_people = victim_player->population * 0.10;
		if (plunder_people > 0)
		{
			victim_player->population -= plunder_people;
			msg->plunder_people = plunder_people;
		}

		// credits
		plunder_credits = victim_player->credits * 0.10;
		if (plunder_credits > 0)
		{
			victim_player->credits -= plunder_credits;
			msg->plunder_credits = plunder_credits;
		}
		// food
		plunder_food = victim_player->food * 0.10;
		if (plunder_food > 0)
		{
			victim_player->food -= plunder_food;
			msg->plunder_food = plunder_food;
		}

		difference = rand() % 5 + 1;
		dtroops = (int)((uint32_t)troops - (uint32_t)((double)troops * ((double)difference / 100.f)));
		dgenerals = (int)((uint32_t)generals - (uint32_t)((double)generals * ((double)difference / 100.f)));
		dfighters = (int)((uint32_t)fighters - (uint32_t)((double)fighters * ((double)difference / 100.f)));

		if (dtroops > victim_player->troops)
			dtroops = victim_player->troops;
		if (dgenerals > victim_player->generals)
			dgenerals = victim_player->generals;
		if (dfighters > victim_player->defence_stations)
			dfighters = victim_player->defence_stations;

		enemy_troops = dtroops;
		enemy_generals = dgenerals;
		enemy_defence_stations = dfighters;

		dtroops = (int)((uint32_t)troops - ((uint32_t)victim_player->troops - (uint32_t)((double)victim_player->troops * ((double)difference / 100.f))));
		dgenerals = (int)((uint32_t)generals - ((uint32_t)victim_player->generals - (uint32_t)((double)victim_player->generals * ((double)difference / 100.f))));
		dfighters = (int)((uint32_t)fighters - ((uint32_t)victim_player->defence_stations - (uint32_t)((double)victim_player->defence_stations * ((double)difference / 100.f))));

		if (dtroops < 0)
			dtroops = 0;
		if (dgenerals < 0)
			dgenerals = 0;
		if (dfighters < 0)
			dfighters = 0;

		msg->troops = dtroops;
		msg->generals = dgenerals;
		msg->fighters = dfighters;

		/*
		msg->troops = troops * victory_chance;
		msg->generals = generals * victory_chance;
		msg->fighters = fighters * victory_chance;

		enemy_troops = (uint32_t)((float)victim_player->troops * (1.f - (float)victory_chance));
		enemy_generals = (uint32_t)((float)victim_player->generals * (1.f - (float)victory_chance));
		enemy_defence_stations = (uint32_t)((float)victim_player->defence_stations * (1.f - (float)victory_chance));
*/

		snprintf(message, 256, "%s from %s attacked you and won, you lost %u citizens, %" PRIu64 " credits, %u food, %u troops, %u generals, %u defence stations.", attacker, bbs_name,
				 plunder_people, plunder_credits, plunder_food, enemy_troops, enemy_generals, enemy_defence_stations);

		victim_player->troops -= enemy_troops;
		victim_player->generals -= enemy_generals;
		victim_player->defence_stations -= enemy_defence_stations;
		msg->score = 1;
	}
	else
	{
		// defeat
		difference = rand() % 5 + 1;
		dtroops = (int)((uint32_t)troops - (uint32_t)((double)troops * ((double)difference / 100.f)));
		dgenerals = (int)((uint32_t)generals - (uint32_t)((double)generals * ((double)difference / 100.f)));
		dfighters = (int)((uint32_t)fighters - (uint32_t)((double)fighters * ((double)difference / 100.f)));

		if (dtroops > victim_player->troops)
			dtroops = victim_player->troops;
		if (dgenerals > victim_player->generals)
			dgenerals = victim_player->generals;
		if (dfighters > victim_player->defence_stations)
			dfighters = victim_player->defence_stations;

		enemy_troops = dtroops;
		enemy_generals = dgenerals;
		enemy_defence_stations = dfighters;

		dtroops = (int)((uint32_t)troops - ((uint32_t)victim_player->troops - (uint32_t)((double)victim_player->troops * ((double)difference / 100.f))));
		dgenerals = (int)((uint32_t)generals - ((uint32_t)victim_player->generals - (uint32_t)((double)victim_player->generals * ((double)difference / 100.f))));
		dfighters = (int)((uint32_t)fighters - ((uint32_t)victim_player->defence_stations - (uint32_t)((double)victim_player->defence_stations * ((double)difference / 100.f))));

		if (dtroops < 0)
			dtroops = 0;
		if (dgenerals < 0)
			dgenerals = 0;
		if (dfighters < 0)
			dfighters = 0;

		msg->troops = dtroops;
		msg->generals = dgenerals;
		msg->fighters = dfighters;

		snprintf(message, 256, "%s from %s attacked you and lost, %u troops, %u generals, %u defence stations were destroyed in the attack.", attacker, bbs_name, enemy_troops, enemy_generals, enemy_defence_stations);
		victim_player->troops -= enemy_troops;
		victim_player->generals -= enemy_generals;
		victim_player->defence_stations -= enemy_defence_stations;
		msg->score = 0;
	}

	dolog("InterBBS Battle: Attack %u, Defence %u, Victory Chance %f, Battle %d", attack, defence, victory_chance, battle);

	send_message(victim_player, NULL, message);

	msg->type = 3;
	msg->from = InterBBSInfo.myNode->nodeNumber;
	strcpy(msg->player_name, attacker);
	strcpy(msg->victim_name, victim);

	msg->created = time(NULL);

	save_player(victim_player);
	free(victim_player);
	return 0;
}

void do_battle(player_t *victim, player_t *attacker, int troops, int generals, int fighters)
{
	char message[256];
	uint32_t attack;
	uint32_t defence;

	int battle;
	int difference;

	uint32_t plunder_people;
	uint64_t plunder_credits;
	uint32_t plunder_food;
	uint64_t plunder_planet_ore;
	uint64_t plunder_planet_industrial;
	uint64_t plunder_planet_military;
	uint64_t plunder_planet_food;

	uint32_t enemy_troops;
	uint32_t enemy_generals;
	uint32_t enemy_defence_stations;
	int dtroops;
	int dgenerals;
	int dfighters;
	float victory_chance;

	// attack soldiers
	attack = (3 * troops + 1 * fighters) + ((3 * troops + 1 * fighters) * (generals / 100));
	defence = 10 * victim->troops;

	// attack defence stations
	attack = attack + (1 * troops + 4 * fighters + ((1 * troops + 4 * fighters) * (generals / 100)));
	defence = defence + (25 * victim->defence_stations);

	victory_chance = ((float)attack / ((float)attack + (float)defence));
	battle = rand() % 100 + 1;

	if (battle < (victory_chance * 100))
	{
		// victory
		md_printf("`white`You are `bright green`victorious`white`, you plundered:\r\n");
		// people
		plunder_people = victim->population * 0.10;
		if (plunder_people > 0)
		{
			victim->population -= plunder_people;
			attacker->population += plunder_people;
			md_printf("   - %u citizens\r\n", plunder_people);
		}

		// credits
		plunder_credits = victim->credits * 0.10;
		if (plunder_credits > 0)
		{
			victim->credits -= plunder_credits;
			attacker->credits += plunder_credits;
			md_printf("   - %" PRIu64 " credits\r\n", plunder_credits);
		}
		// food
		plunder_food = victim->food * 0.10;
		if (plunder_food > 0)
		{
			victim->food -= plunder_food;
			attacker->food += plunder_food;
			md_printf("   - %u food\r\n", plunder_food);
		}
		// planets
		plunder_planet_ore = victim->planets_ore * 0.05;
		if (plunder_planet_ore > 0)
		{
			victim->planets_ore -= plunder_planet_ore;
			attacker->planets_ore += plunder_planet_ore;
			md_printf("   - %u ore planets\r\n", plunder_planet_ore);
		}
		plunder_planet_food = victim->planets_food * 0.05;
		if (plunder_planet_food > 0)
		{
			victim->planets_food -= plunder_planet_food;
			attacker->planets_food += plunder_planet_food;
			md_printf("   - %u food planets\r\n", plunder_planet_food);
		}
		plunder_planet_industrial = victim->planets_industrial * 0.05;
		if (plunder_planet_industrial > 0)
		{
			victim->planets_industrial -= plunder_planet_industrial;
			attacker->planets_industrial += plunder_planet_industrial;
			md_printf("   - %u industrial planets\r\n", plunder_planet_industrial);
		}
		plunder_planet_military = victim->planets_military * 0.05;
		if (plunder_planet_military > 0)
		{
			victim->planets_military -= plunder_planet_military;
			attacker->planets_military += plunder_planet_military;
			md_printf("   - %u soldier planets\r\n", plunder_planet_military);
		}

		difference = rand() % 5 + 1;
		dtroops = (int)((uint32_t)troops - (uint32_t)((double)troops * ((double)difference / 100.f)));
		dgenerals = (int)((uint32_t)generals - (uint32_t)((double)generals * ((double)difference / 100.f)));
		dfighters = (int)((uint32_t)fighters - (uint32_t)((double)fighters * ((double)difference / 100.f)));

		if (dtroops > victim->troops)
			dtroops = victim->troops;
		if (dgenerals > victim->generals)
			dgenerals = victim->generals;
		if (dfighters > victim->defence_stations)
			dfighters = victim->defence_stations;

		enemy_troops = dtroops;
		enemy_generals = dgenerals;
		enemy_defence_stations = dfighters;

		dtroops = (int)((uint32_t)troops - ((uint32_t)victim->troops - (uint32_t)((double)victim->troops * ((double)difference / 100.f))));
		dgenerals = (int)((uint32_t)generals - ((uint32_t)victim->generals - (uint32_t)((double)victim->generals * ((double)difference / 100.f))));
		dfighters = (int)((uint32_t)fighters - ((uint32_t)victim->defence_stations - (uint32_t)((double)victim->defence_stations * ((double)difference / 100.f))));

		if (dtroops < 0)
			dtroops = 0;
		if (dgenerals < 0)
			dgenerals = 0;
		if (dfighters < 0)
			dfighters = 0;

		troops = dtroops;
		generals = dgenerals;
		fighters = dfighters;

		/*
		if (victory_chance > 0.75) {
			victory_chance = 0.75;
		}
		
		
		
		troops = troops * victory_chance;
		generals = generals * victory_chance;
		fighters = fighters * victory_chance;

		enemy_troops = victim->troops * victory_chance;
		enemy_generals = victim->generals * victory_chance;
		enemy_defence_stations = victim->defence_stations * victory_chance;
*/
		snprintf(message, 256, "%s attacked you and won, you lost %u citizens, %" PRIu64 " credits, %u food, %" PRIu64 " planets (%" PRIu64 " ore, %" PRIu64 " industrial, %" PRIu64 " soldier, %" PRIu64 " food), %u troops, %u generals, %u defence stations.", attacker->gamename,
				 plunder_people, plunder_credits, plunder_food, plunder_planet_food + plunder_planet_military + plunder_planet_industrial + plunder_planet_ore, plunder_planet_ore, plunder_planet_industrial, plunder_planet_military, plunder_planet_food, victim->troops - enemy_troops, victim->generals - enemy_generals, victim->defence_stations - enemy_defence_stations);
	}
	else
	{
		// defeat

		difference = rand() % 5 + 1;
		dtroops = (int)((uint32_t)troops - (uint32_t)((double)troops * ((double)difference / 100.f)));
		dgenerals = (int)((uint32_t)generals - (uint32_t)((double)generals * ((double)difference / 100.f)));
		dfighters = (int)((uint32_t)fighters - (uint32_t)((double)fighters * ((double)difference / 100.f)));

		if (dtroops > victim->troops)
			dtroops = victim->troops;
		if (dgenerals > victim->generals)
			dgenerals = victim->generals;
		if (dfighters > victim->defence_stations)
			dfighters = victim->defence_stations;

		enemy_troops = dtroops;
		enemy_generals = dgenerals;
		enemy_defence_stations = dfighters;

		dtroops = (int)((uint32_t)troops - ((uint32_t)victim->troops - (uint32_t)((double)victim->troops * ((double)difference / 100.f))));
		dgenerals = (int)((uint32_t)generals - ((uint32_t)victim->generals - (uint32_t)((double)victim->generals * ((double)difference / 100.f))));
		dfighters = (int)((uint32_t)fighters - ((uint32_t)victim->defence_stations - (uint32_t)((double)victim->defence_stations * ((double)difference / 100.f))));

		if (dtroops < 0)
			dtroops = 0;
		if (dgenerals < 0)
			dgenerals = 0;
		if (dfighters < 0)
			dfighters = 0;

		troops = dtroops;
		generals = dgenerals;
		fighters = dfighters;
		/*
		if (victory_chance > 0.75) {
			victory_chance = 0.75;
		}

		troops = troops * victory_chance;
		generals = generals * victory_chance;
		fighters = fighters * victory_chance;

		enemy_troops = victim->troops * victory_chance;
		enemy_generals = victim->generals * victory_chance;
		enemy_defence_stations = victim->defence_stations * victory_chance;
*/
		md_printf("`white`You are `bright red`defeated`white`.\r\n");
		snprintf(message, 256, "%s attacked you and lost, %u troops, %u generals, %u defence stations were destroyed in the attack", attacker->gamename, victim->troops - enemy_troops, victim->generals - enemy_generals, victim->defence_stations - enemy_defence_stations);
	}

	send_message(victim, NULL, message);

	md_printf(" %u troops, %u generals and %u fighters return home.\r\n", troops, generals, fighters);
	md_printf(" %u enemy troops, %u enemy generals and %u enemy defence stations were destroyed\r\n", enemy_troops, enemy_generals, enemy_defence_stations);

	md_printf("\r\nPress a key to continue\r\n");
	md_getc();

	attacker->troops += troops;
	attacker->generals += generals;
	attacker->fighters += fighters;

	victim->troops -= enemy_troops;
	victim->generals -= enemy_generals;
	victim->defence_stations -= enemy_defence_stations;
}

void add_link(int link, const char* linkname) {
	FILE* fptr;
	fptr = fopen(FILEEXT "-IBBS.CFG", "a");
	if (!fptr)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG");
		return;
	}
	fprintf(fptr, "\nLinkNodeNumber %d\n", link);
	fprintf(fptr, "LinkName %s\n", linkname);
	fclose(fptr);

}

void remove_link(int link) {
	FILE *fptr;
	FILE *fptr2;
	char message[256];
	char message2[256];
	int stage = 0;

	dolog("Removing link %d from game", link);

	fptr = fopen(FILEEXT "-IBBS.CFG", "r");
	if (!fptr)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG");
		return;
	}

	fptr2 = fopen(FILEEXT "-IBBS.CFG.BAK", "w");
	if (!fptr2)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG.BAK");
		return;
	}
	fgets(message, 256, fptr);
	while (!feof(fptr))
	{
		fputs(message, fptr2);
		fgets(message, 256, fptr);
	}
	fclose(fptr2);
	fclose(fptr);
	fptr = fopen(FILEEXT "-IBBS.CFG.BAK", "r");
	if (!fptr)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG.BAK");
		return;
	}

	fptr2 = fopen(FILEEXT "-IBBS.CFG", "w");
	if (!fptr2)
	{
		dolog("Unable to open " FILEEXT "-IBBS.CFG");
		return;
	}

	sprintf(message2, "LinkNodeNumber %d", link);

	fgets(message, 256, fptr);
	while (!feof(fptr))
	{
		if (strncasecmp(message, message2, strlen(message2)) == 0)
		{
			stage = 1;
		}
		else if (strncasecmp(message, "LinkNodeNumber", 14) == 0 && stage == 1)
		{
			stage = 0;
		}
		if (stage == 0 || message[0] == ';')
		{
			fputs(message, fptr2);
		}
		else {
			fprintf(fptr2, "; DELETED %s", message);
		}
		fgets(message, 256, fptr);
	}
	fclose(fptr2);
	fclose(fptr);
	unlink(FILEEXT "-IBBS.CFG.BAK");

}

void perform_maintenance()
{
	ibbsmsg_t msg;
	ibbsmsg_t outboundmsg;
	int i;
	int id;
	int rc;
	sqlite3_stmt *stmt;
	sqlite3 *db;
	char sqlbuffer[256];
	char **players = NULL;
	player_t *player;
	int j;
	int k;
	int z;
	time_t last_score;
	tIBResult result;
	char message[256];
	FILE *fptr, *fptr2;
	int newnodenum;
	uint32_t newgameid;
	char buffer[256];
	int reset = 0;
	struct stat st;
	time_t last_recon;

	dolog("Performing maintenance...");

	// parse all incoming messages
	i = 0;
	k = 0;
	if (interBBSMode == 1)
	{
		while (1)
		{
			result = IBGet(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));

			if (result == eSuccess)
			{
				msg2he(&msg);
				if ((msg.turns_in_protection != turns_in_protection || msg.turns_per_day != turns_per_day) && msg.from != 1)
				{
					dolog("Settings mismatch. Ignoring packet from node: %d (turns in protection %d, turns per day %d)", msg.from, msg.turns_in_protection, msg.turns_per_day);
					continue;
				}
				else if ((msg.turns_in_protection != turns_in_protection || msg.turns_per_day != turns_per_day) && msg.from == 1)
				{
					fptr = fopen("galactic.ini", "w");
					if (!fptr)
					{
						dolog("Unable to open galactic.ini");
					}
					else
					{
						fprintf(fptr, "[Main]\n");
						fprintf(fptr, "Turns in Protection = %d\n", msg.turns_in_protection);
						fprintf(fptr, "Turns per Day = %d\n", msg.turns_per_day);
						if (log_path != NULL)
						{
							fprintf(fptr, "Log Path = %s\n", log_path);
						}
						else
						{
							fprintf(fptr, ";Log Path = logs\n");
						}
						if (bad_path != NULL)
						{
							fprintf(fptr, "Bad Path = %s\n", bad_path);
						}
						else
						{
							fprintf(fptr, ";Bad Path = bad\n");
						}

						fprintf(fptr, "\n");
						fprintf(fptr, "[InterBBS]\n");
						fprintf(fptr, "Enabled = True\n");
						fprintf(fptr, "System Name = %s\n", InterBBSInfo.myNode->name);
						fprintf(fptr, "Node Number = %d\n", InterBBSInfo.myNode->nodeNumber);
						fprintf(fptr, "League Number = %d\n", InterBBSInfo.league);
						fprintf(fptr, "File Inbox = %s\n", InterBBSInfo.myNode->filebox);
						fprintf(fptr, "Default Outbox = %s\n", InterBBSInfo.defaultFilebox);
						fclose(fptr);

						turns_in_protection = msg.turns_in_protection;
						turns_per_day = msg.turns_per_day;
					}
				}
				memset(&outboundmsg, 0, sizeof(ibbsmsg_t));
				switch (msg.type)
				{
				case 1:
					// add score to database
					dolog("Got score file packet for player %s", msg.player_name);
					if (!open_interbbs_database(&db))
					{
						// Error opening the database
						dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
						return;
					}
					snprintf(sqlbuffer, 256, "SELECT id, last FROM scores WHERE gamename=? and address=?");
					sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
					sqlite3_bind_text(stmt, 1, msg.player_name, strlen(msg.player_name), SQLITE_STATIC);
					sqlite3_bind_int(stmt, 2, msg.from);

					rc = sqlite3_step(stmt);

					if (rc == SQLITE_ROW)
					{
						id = sqlite3_column_int(stmt, 0);
						last_score = sqlite3_column_int(stmt, 1);
						sqlite3_finalize(stmt);
						if (last_score < msg.created)
						{
							snprintf(sqlbuffer, 256, "UPDATE scores SET score=?, last=? WHERE id=?");
							sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
							sqlite3_bind_int(stmt, 1, msg.score);
							sqlite3_bind_int(stmt, 2, msg.created);
							sqlite3_bind_int(stmt, 3, id);
							sqlite3_step(stmt);
							sqlite3_finalize(stmt);
						}
					}
					else
					{
						sqlite3_finalize(stmt);
						snprintf(sqlbuffer, 256, "INSERT INTO scores (address, gamename, score, last) VALUES(?, ?, ?, ?)");
						sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
						sqlite3_bind_int(stmt, 1, msg.from);
						sqlite3_bind_text(stmt, 2, msg.player_name, strlen(msg.player_name), SQLITE_STATIC);
						sqlite3_bind_int(stmt, 3, msg.score);
						sqlite3_bind_int(stmt, 4, msg.created);
						sqlite3_step(stmt);
						sqlite3_finalize(stmt);
					}
					sqlite3_close(db);

					break;
				case 2:
					dolog("Got invasion packet for: %s from: %s", msg.victim_name, msg.player_name);
					// perform invasion
					if (do_interbbs_battle(msg.victim_name, msg.player_name, msg.from, msg.troops, msg.generals, msg.fighters, &outboundmsg) == 0)
					{
						outboundmsg.turns_in_protection = turns_in_protection;
						outboundmsg.turns_per_day = turns_per_day;
						msg2ne(&outboundmsg);
						IBSend(&InterBBSInfo, msg.from, &outboundmsg, sizeof(ibbsmsg_t));
					}
					else
					{
						dolog("Invasion failed");
					}
					break;
				case 3:
					// return troops
					dolog("Got return troops packet for: %s", msg.player_name);
					player = load_player_gn(msg.player_name);
					if (player != NULL)
					{
						player->troops += msg.troops;
						player->generals += msg.generals;
						player->fighters += msg.fighters;

						if (msg.score == 1)
						{
							player->population += msg.plunder_people;
							player->credits += msg.plunder_credits;
							player->food += msg.plunder_food;
							snprintf(message, 256, "Your armada returned victorious, %u troops, %u generals and %u fighters returned with %u prisoners, %" PRIu64 " credits and %u food.",
									 msg.troops, msg.generals, msg.fighters, msg.plunder_people, msg.plunder_credits, msg.plunder_food);
						}
						else if (msg.score == 0)
						{
							snprintf(message, 256, "Your armada returned defeated, %u troops, %u generals and %u fighters returned.",
									 msg.troops, msg.generals, msg.fighters);
						}
						else
						{
							snprintf(message, 256, "Your armada encounted galactic protection and all your troops returned disappointed.");
						}
						send_message(player, NULL, message);
						save_player(player);
						free(player);
					}
					else
					{
						dolog("return troops failed");
					}
					break;
				case 4:
					// message
					if (!open_interbbs_database(&db))
					{
						// Error opening the database
						dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
						return;
					}
					snprintf(sqlbuffer, 256, "INSERT INTO messages (recipient, 'from', address, date, seen, body) VALUES(?, ?, ?, ?, ?, ?)");
					sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer), &stmt, NULL);
					sqlite3_bind_text(stmt, 1, msg.victim_name, strlen(msg.victim_name), SQLITE_STATIC);
					sqlite3_bind_text(stmt, 2, msg.player_name, strlen(msg.player_name), SQLITE_STATIC);
					sqlite3_bind_int(stmt, 3, msg.from);
					sqlite3_bind_int(stmt, 4, msg.created);
					sqlite3_bind_int(stmt, 5, 0);
					sqlite3_bind_text(stmt, 6, msg.message, strlen(msg.message), SQLITE_STATIC);

					sqlite3_step(stmt);
					sqlite3_finalize(stmt);

					sqlite3_close(db);
					break;
				case 5:
					// new node
					if (msg.from != 1)
					{
						dolog("Received ADD/REMOVE from system not Node 1");
						break;
					}
					if (strcmp(msg.victim_name, "ADD") == 0)
					{
						dolog("Received ADD Node Packet");
						newnodenum = atoi(msg.player_name);
						remove_link(newnodenum);
						add_link(newnodenum, msg.message);
					}
					else if (strcmp(msg.victim_name, "REMOVE"))
					{
						dolog("Received REMOVE Node Packet");
						newnodenum = atoi(msg.player_name);
						remove_link(newnodenum);
					}
					break;
				case 6:
					if (stat("ibbs_winner.dat", &st) != 0)
					{
						fptr = fopen("ibbs_winner.dat", "w");
						fprintf(fptr, "%s\n", msg.player_name);
						fprintf(fptr, "%u\n", msg.from);
						fclose(fptr);
						for (z = 0; z < InterBBSInfo.otherNodeCount; z++)
						{
							if (msg.from == InterBBSInfo.otherNodes[z]->nodeNumber)
							{
								snprintf(buffer, 256, "Queen Mapa destroyed! %s of %s is the new System Lord!", msg.player_name, InterBBSInfo.otherNodes[z]->name);
								send_system_message(buffer);
								break;
							}
						}
					}
					break;
				case 7:
					if (msg.from == 1)
					{
						newgameid = atoi(msg.player_name);
						fptr = fopen(FILEEXT "-IBBS.CFG", "r");
						if (!fptr)
						{
							dolog("Unable to open " FILEEXT "-IBBS.CFG");
							break;
						}

						fptr2 = fopen(FILEEXT "-IBBS.CFG.BAK", "w");
						if (!fptr2)
						{
							dolog("Unable to open " FILEEXT "-IBBS.CFG.BAK");
							break;
						}
						fgets(message, 256, fptr);
						while (!feof(fptr))
						{
							fputs(message, fptr2);
							fgets(message, 256, fptr);
						}
						fclose(fptr2);
						fclose(fptr);

						fptr = fopen(FILEEXT "-IBBS.CFG.BAK", "r");
						if (!fptr)
						{
							dolog("Unable to open " FILEEXT "-IBBS.CFG.BAK");
							break;
						}

						fptr2 = fopen(FILEEXT "-IBBS.CFG", "w");
						if (!fptr2)
						{
							dolog("Unable to open " FILEEXT "-IBBS.CFG");
							break;
						}

						fgets(message, 256, fptr);
						while (!feof(fptr))
						{
							if (strncasecmp(message, "GameID", 6) == 0)
							{
								fprintf(fptr2, "GameID %d\n", newgameid);
							}
							else
							{
								fputs(message, fptr2);
							}
							fgets(message, 256, fptr);
						}
						fclose(fptr2);
						fclose(fptr);
						unlink(FILEEXT "-IBBS.CFG.BAK");

						reset = 1;
					}
					else
					{
						dolog("Got game id change from someone not node 1, ignoring");
					}
					break;
				case 8:
					// long range spying
					dolog("Got long range spy packet for: %s from: %s", msg.victim_name, msg.player_name);
					// perform invasion
					if (do_longrange_spy(msg.victim_name, msg.player_name, msg.from, &outboundmsg) == 0)
					{
						outboundmsg.turns_in_protection = turns_in_protection;
						outboundmsg.turns_per_day = turns_per_day;
						msg2ne(&outboundmsg);
						IBSend(&InterBBSInfo, msg.from, &outboundmsg, sizeof(ibbsmsg_t));
					}
					else
					{
						dolog("Long range spying failed");
					}
					break;
				case 9:
					// sabotage planets
					dolog("Got long range sabotage packet for: %s from: %s", msg.victim_name, msg.player_name);
					// perform invasion
					if (do_longrange_sabotage(msg.victim_name, msg.player_name, msg.from, msg.generals, &outboundmsg) == 0)
					{
						outboundmsg.turns_in_protection = turns_in_protection;
						outboundmsg.turns_per_day = turns_per_day;
						msg2ne(&outboundmsg);
						IBSend(&InterBBSInfo, msg.from, &outboundmsg, sizeof(ibbsmsg_t));
					}
					else
					{
						dolog("Long range sabotage failed");
					}
					break;
				case 10:
					// return long range spying
					player = load_player_gn(msg.player_name);
					if (player != NULL)
					{
						if (msg.score == 0)
						{
							// spy was caught
							snprintf(message, 256, "Your spy was caught infiltrating %s and was brutally executed.", msg.victim_name);
						}
						else
						{
							// spy was successful
							snprintf(message, 256, "Your spy was successful infiltrating %s\r\n\r\nPlanets: %u\r\nPopulation: %u\r\nCredits: %" PRIu64 "\r\nTroops: %u\r\nGenerals: %u\r\nDefence Stations:%u\r\n.", msg.victim_name, msg.plunder_food, msg.plunder_people, msg.plunder_credits, msg.troops, msg.generals, msg.fighters);
							player->spies++;
						}
						send_message(player, NULL, message);
						save_player(player);
						free(player);
					}
					else
					{
						dolog("Long range spy return failed..");
					}
					break;
				case 11:
					// return long range sabotage
					player = load_player_gn(msg.player_name);
					if (player != NULL)
					{
						snprintf(message, 256, "%u of %u spies returned from %s, having destroyed %u planets.", msg.fighters, msg.troops, msg.victim_name, msg.generals);
						send_message(player, NULL, message);
						player->spies += msg.fighters;
						save_player(player);
						free(player);
					}
					else
					{
						dolog("Long range spy return failed..");
					}
					break;
				case 12:
					// score recon packet
					// send all score messages
					if (!open_users_database(&db))
					{
						// Error opening the database
						dolog("Error opening user database: %s", sqlite3_errmsg(db));
						exit(1);
					}
					snprintf(sqlbuffer, 256, "SELECT gamename FROM users;");
					sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);

					i = 0;
					rc = sqlite3_step(stmt);
					while (rc == SQLITE_ROW)
					{
						if (i == 0)
						{
							players = (char **)malloc(sizeof(char *));
						}
						else
						{
							players = (char **)realloc(players, sizeof(char *) * (i + 1));
						}
						players[i] = (char *)malloc(sizeof(char) * 17);
						strcpy(players[i], (char *)sqlite3_column_text(stmt, 0));
						i++;
						rc = sqlite3_step(stmt);
					}

					sqlite3_finalize(stmt);
					sqlite3_close(db);

					for (j = 0; j < i; j++)
					{
						player = load_player_gn(players[j]);
						if (player != NULL)
						{
							memset(&outboundmsg, 0, sizeof(ibbsmsg_t));
							outboundmsg.type = 1;
							outboundmsg.from = InterBBSInfo.myNode->nodeNumber;
							strcpy(outboundmsg.player_name, player->gamename);
							outboundmsg.score = calculate_score(player);
							outboundmsg.created = time(NULL);
							player->last_score = outboundmsg.score;
							save_player(player);
							outboundmsg.turns_in_protection = turns_in_protection;
							outboundmsg.turns_per_day = turns_per_day;
							msg2ne(&outboundmsg);
							if (IBSend(&InterBBSInfo, msg.from, &outboundmsg, sizeof(ibbsmsg_t)) != eSuccess)
							{
								dolog("Unable to write score packet to outbound.");
							}
							free(player);
						}
						free(players[j]);
					}
					if (i > 0)
					{
						free(players);
					}
					break;
				default:
					dolog("Unknown message type: %d", msg.type);
					break;
				}
				i++;
			}
			else if (result == eForwarded)
			{
				k++;
			}
			else
			{
				break;
			}
		}
		if (reset == 1)
		{
			dolog("Got reset message! resetting the game...");
#if defined(_MSC_VER) || defined(WIN32)
			system("reset.bat");
#else
			system("./reset.sh");
#endif
			if (unlink("inuse.flg") != 0)
			{
				perror("unlink ");
			}

			exit(0);
		}
		dolog("Parsed %d inbound messages; Forwarded %d messages", i, k);

		for (i = 0; i < InterBBSInfo.otherNodeCount; i++)
		{
			if (InterBBSInfo.otherNodes[i]->nodeNumber == InterBBSInfo.myNode->nodeNumber)
			{
				continue;
			}
			if (!open_interbbs_database(&db))
			{
				// Error opening the database
				dolog("Error opening interbbs database: %s", sqlite3_errmsg(db));
				exit(1);
			}
			snprintf(sqlbuffer, 256, "SELECT * FROM recon WHERE last > ? AND address = ?;");
			sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
			last_recon = time(NULL) - 43200;
			sqlite3_bind_int(stmt, 1, last_recon);
			sqlite3_bind_int(stmt, 2, InterBBSInfo.otherNodes[i]->nodeNumber);
			if (sqlite3_step(stmt) != SQLITE_ROW)
			{
				sqlite3_finalize(stmt);

				// send recon packet
				dolog("Sending score RECON packet to node %d", InterBBSInfo.otherNodes[i]->nodeNumber);
				memset(&outboundmsg, 0, sizeof(ibbsmsg_t));
				outboundmsg.type = 12;
				outboundmsg.from = InterBBSInfo.myNode->nodeNumber;
				outboundmsg.created = time(NULL);
				outboundmsg.turns_in_protection = turns_in_protection;
				outboundmsg.turns_per_day = turns_per_day;
				msg2ne(&outboundmsg);
				if (IBSend(&InterBBSInfo, InterBBSInfo.otherNodes[i]->nodeNumber, &outboundmsg, sizeof(ibbsmsg_t)) != eSuccess)
				{
					dolog("Unable to write recon packet to outbound.");
				}
				// check if node is in recon list
				snprintf(sqlbuffer, 256, "SELECT * FROM recon WHERE address = ?;");
				sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
				sqlite3_bind_int(stmt, 1, InterBBSInfo.otherNodes[i]->nodeNumber);
				if (sqlite3_step(stmt) == SQLITE_ROW)
				{
					sqlite3_finalize(stmt);
					snprintf(sqlbuffer, 256, "UPDATE recon SET last = ? WHERE address = ?");
					sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
					last_recon = time(NULL);
					sqlite3_bind_int(stmt, 1, last_recon);
					sqlite3_bind_int(stmt, 2, InterBBSInfo.otherNodes[i]->nodeNumber);
					sqlite3_step(stmt);
				}
				else
				{
					sqlite3_finalize(stmt);
					snprintf(sqlbuffer, 256, "INSERT into recon (last, address) VALUES(?, ?)");
					sqlite3_prepare_v2(db, sqlbuffer, strlen(sqlbuffer) + 1, &stmt, NULL);
					last_recon = time(NULL);
					sqlite3_bind_int(stmt, 1, last_recon);
					sqlite3_bind_int(stmt, 2, InterBBSInfo.otherNodes[i]->nodeNumber);
					sqlite3_step(stmt);
				}
			}
			sqlite3_finalize(stmt);
			sqlite3_close(db);
		}
		// build global top scores
		build_interbbs_scorefile();
	}
}

void game_won(player_t *player)
{
	char buffer[256];
	ibbsmsg_t msg;
	FILE *fptr;

	fptr = fopen("ibbs_winner.dat", "w");
	if (!fptr)
	{
		return;
	}
	fprintf(fptr, "%s\n", player->gamename);

	if (interBBSMode == 1)
	{
		fprintf(fptr, "%ul\n", InterBBSInfo.myNode->nodeNumber);
	}

	fclose(fptr);

	if (interBBSMode == 1)
	{
		memset(&msg, 0, sizeof(ibbsmsg_t));
		msg.type = 6;
		msg.from = InterBBSInfo.myNode->nodeNumber;
		msg.turns_per_day = turns_per_day;
		msg.turns_in_protection = turns_in_protection;
		strcpy(msg.player_name, player->gamename);
		msg.created = time(NULL);
		msg2ne(&msg);
		IBSendAll(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));
	}
	snprintf(buffer, 256, "Queen Mapa destroyed! %s of this Galaxy is the new System Lord!", player->gamename);

	send_system_message(buffer);
}

void state_of_the_galaxy(player_t *player)
{
	md_printf("\r\n`bright blue`============================================================\r\n");
	md_printf(" `white`State of the Empire.               Today's turns left %d\r\n", player->turns_left);
	md_printf("`bright blue`============================================================\r\n");
	md_printf("`white` - Score        : %" PRIu64 "\r\n", calculate_score(player));
	md_printf(" - Population   : %u million\r\n", player->population);
	md_printf(" - Food         : %u tonnes\r\n", player->food);
	md_printf(" - Credits      : %" PRIu64 "\r\n", player->credits);
	md_printf(" - Ore          : %u\r\n", player->ore);
	md_printf(" - Sprockets    : %u\r\n", player->sprockets);
	md_printf(" - Troops       : %u\r\n", player->troops);
	md_printf(" - Generals     : %u\r\n", player->generals);
	md_printf(" - Fighters     : %u\r\n", player->fighters);
	md_printf(" - Spies        : %u\r\n", player->spies);
	md_printf(" - Def. Stations: %u\r\n", player->defence_stations);
	md_printf(" - Command Ship : %d%% complete\r\n", player->command_ship);
	md_printf(" - Planets      : %u\r\n", player->planets_food + player->planets_ore + player->planets_military + player->planets_industrial + player->planets_urban);
	md_printf("   (Ore %u) (Food %u) (Soldier %u) (Industrial %u) (Urban %u)\r\n", player->planets_ore, player->planets_food, player->planets_military, player->planets_industrial, player->planets_urban);
	if (player->total_turns < turns_in_protection)
	{
		md_printf("`bright yellow`You have %d turns left under protection.\r\n", turns_in_protection - player->total_turns);
	}
	md_printf("`bright blue`============================================================`white`\r\n");
}

player_t *select_victim(player_t *player, char *prompt, int type)
{
	char gamename[17];
	player_t *victim;

	while (1)
	{
		md_printf("\r\n%s ('?' for a list, ENTER to cancel) ? ", prompt);
		md_getstring(gamename, 17, 32, 126);
		if (strlen(gamename) == 1 && gamename[0] == '?')
		{
			list_empires(player);
		}
		else if (strlen(gamename) == 0)
		{
			return NULL;
		}
		else
		{
			victim = load_player_gn(gamename);
			if (victim == NULL)
			{
				md_printf("\r\nNo such empire!\r\n");
			}
			else if (victim->id == player->id)
			{
				if (type == 1)
				{
					md_printf("\r\nYou can't send a message to yourself\r\n'");
				}
				else if (type == 2)
				{
					md_printf("\r\nYou can't attack yourself!\r\n");
				}
				else if (type == 3)
				{
					md_printf("\r\nYou can't spy on yourself!\r\n");
				}
			}
			else if (victim->total_turns < turns_in_protection && type > 1)
			{
				md_printf("\r\nSorry, that empire is under protection.\r\n");
			}
			else
			{
				return victim;
			}
		}
	}
	return NULL;
}

void game_loop(player_t *player)
{
	uint64_t tribute;
	uint32_t troop_wages;
	uint32_t citizen_hunger;
	uint32_t total_industrial;
	uint32_t total_ore;
	int done;
	int bank_done;
	float starvation;
	float loyalty;

	char message[256];
	char buffer[10];
	ibbsmsg_t msg;
	int addr;

	player_t *victim;

	int64_t i;
	char c;

	uint32_t troops_to_send;
	uint32_t generals_to_send;
	uint32_t fighters_to_send;
	uint32_t spies_to_send;

	struct stat st;
	unseen_msgs(player);

	if (interBBSMode == 1)
	{
		unseen_ibbs_msgs(player);
	}

	while (player->turns_left)
	{

		// Diplomatic Stage
		done = 0;
		while (done == 0)
		{
			md_printf("\r\n`bright cyan`============================================================\r\n");
			md_printf("`white` Diplomatic Relations\r\n");
			md_printf("`bright cyan`============================================================`white`\r\n");
			md_printf("  (1) Send a sub-space message\r\n");
			if (interBBSMode == 1)
			{
				md_printf("  (2) Send an inter-galactic message\r\n");
			}
			md_printf("  (`bright green`D`white`) Done\r\n");
			md_printf("`bright cyan`============================================================`white`\r\n");
			if (interBBSMode == 1)
			{
				c = md_get_answer("12dD\r");
			}
			else
			{
				c = md_get_answer("1dD\r");
			}
			switch (c)
			{
			case '1':
				victim = select_victim(player, "Who do you want to message", 1);
				if (victim != NULL)
				{
					md_printf("Type your message (256 chars max)\r\n");
					md_getstring(message, 256, 32, 126);
					if (strlen(message) > 0)
					{
						send_message(victim, player, message);
						md_printf("\r\nMessage sent!\r\n");
					}
					else
					{
						md_printf("\r\nNot sending an empty message.\r\n");
					}
					free(victim);
				}
				break;
			case '2':
				addr = select_bbs(2);
				if (addr != 256)
				{
					memset(&msg, 0, sizeof(ibbsmsg_t));
					if (select_ibbs_player(addr, msg.victim_name) == 0)
					{
						md_printf("Type your message (256 chars max)\r\n");
						md_getstring(msg.message, 256, 32, 126);
						if (strlen(msg.message) > 0)
						{
							msg.type = 4;
							strcpy(msg.player_name, player->gamename);
							msg.from = InterBBSInfo.myNode->nodeNumber;
							msg.created = time(NULL);
							msg.turns_in_protection = turns_in_protection;
							msg.turns_per_day = turns_per_day;
							msg2ne(&msg);
							if (IBSend(&InterBBSInfo, addr, &msg, sizeof(ibbsmsg_t)) != eSuccess)
							{
								md_printf("\r\nMessage failed to send.\r\n");
							}
							else
							{
								md_printf("\r\nMessage sent!\r\n");
							}
						}
						else
						{
							md_printf("\r\nNot sending an empty message.\r\n");
						}
					}
				}
				break;
			default:
				done = 1;
				break;
			}
		}
		// State of the Galaxy
		state_of_the_galaxy(player);
        if (stat("ibbs_winner.dat", &st) != 0) {
            if (interBBSMode)
            {
                if (is_highest_score_player_ibbs() && turns_in_protection <= gPlayer->total_turns)
                {
                    i = rand() % 100;

                    if (i > 75)
                    {
                        tribute = (player->credits + (player->bank_balance > 0 ? player->bank_balance : 0)) * 0.10f;
                        if (tribute > 0)
                        {
                            md_printf("\r\n`bright magenta`Queen Mapa`white` demands tribute! Pay 10%% of your credits or be obliterated!\r\n");
                            if (tribute > player->credits)
                            {
                                md_printf("You pay %" PRIu64 " credits of hand, and %" PRIu64 " from your bank.\r\n", player->credits, tribute - player->credits);
                                player->bank_balance -= (tribute - player->credits);
                                player->credits = 0;
                            }
                            else
                            {
                                md_printf("You pay %" PRIu64 " credits.\r\n", tribute);
                                player->credits -= tribute;
                            }
                        }
                    }
                }
            }
            else
            {
                if (is_highest_score_player() && turns_in_protection <= gPlayer->total_turns)
                {
                    i = rand() % 100;

                    if (i > 75)
                    {
                        tribute = (player->credits + (player->bank_balance > 0 ? player->bank_balance : 0)) * 0.10f;
                        if (tribute > 0)
                        {
                            md_printf("\r\n`bright magenta`Queen Mapa`white` demands tribute! Pay 10%% of your credits or be obliterated!\r\n");
                            if (tribute > player->credits)
                            {
                                md_printf("You pay %" PRIu64 " credits of hand, and %" PRIu64 " from your bank.\r\n", player->credits, tribute - player->credits);
                                player->bank_balance -= (tribute - player->credits);
                                player->credits = 0;
                            }
                            else
                            {
                                md_printf("You pay %" PRIu64 " credits.\r\n", tribute);
                                player->credits -= tribute;
                            }
                        }
                    }
                }
            }
        }
		// Troops require money
		troop_wages = player->troops * 10 + player->generals * 15 + player->fighters * 20 + player->defence_stations * 20;
		md_printf("Your military requires `bright yellow`%u`white` credits in wages.\r\n", troop_wages);
		md_printf("Pay them (`bright green`%" PRIu64 "`white`) : ", (troop_wages < player->credits ? troop_wages : player->credits));

		md_getstring(buffer, 8, '0', '9');

		if (strlen(buffer) == 0)
		{
			i = (troop_wages < player->credits ? troop_wages : player->credits);
		}
		else
		{
			i = atoi(buffer);
			if (i > player->credits)
			{
				i = player->credits;
			}
		}

		if (i < troop_wages)
		{
			md_printf("\r\n`bright red`Warning! `white`Your troops will not be impressed!\r\n");
		}
		else
		{
			i = troop_wages;
		}

		loyalty = (float)i / (float)troop_wages;
		player->credits -= i;

		// People require food
		citizen_hunger = (player->population / 10) + 1;
		md_printf("\r\nYour citizens need `bright yellow`%u`white` tonnes of food.\r\n", citizen_hunger);
		md_printf("Feed them (`bright green`%u`white`) : ", (citizen_hunger < player->food ? citizen_hunger : player->food));

		md_getstring(buffer, 8, '0', '9');

		if (strlen(buffer) == 0)
		{
			i = (citizen_hunger < player->food ? citizen_hunger : player->food);
		}
		else
		{
			i = atoi(buffer);
			if (i > player->food)
			{
				i = player->food;
			}
		}

		if (i < citizen_hunger)
		{
			md_printf("\r\n`bright red`Warning! `white`Your citizens will starve!\r\n");
		}
		else
		{
			i = citizen_hunger;
		}

		starvation = (float)i / (float)citizen_hunger;
		player->food -= i;

		done = 0;
		while (done == 0)
		{
			// do you want to buy anything
			md_printf("\r\n`bright green`============================================================\r\n");
			md_printf("`white` Buy Stuff                 Your funds: %" PRIu64 " credits\r\n", player->credits);
			md_printf("`bright green`============================`green`[`white`Price`green`]`bright green`=`green`[`white`You Have`green`]`bright green`=`green`[`white`Can Afford`green`]`bright green`=\r\n");
			md_printf("`white` (1) Troops                    100    %6u     %6" PRIu64 "\r\n", player->troops, player->credits / 100);
			md_printf(" (2) Generals                  500    %6u     %6" PRIu64 "\r\n", player->generals, player->credits / 500);
			md_printf(" (3) Fighters                 1000    %6u     %6" PRIu64 "\r\n", player->fighters, player->credits / 1000);
			md_printf(" (4) Defence Stations         1000    %6u     %6" PRIu64 "\r\n", player->defence_stations, player->credits / 1000);
			if (player->command_ship == 100)
			{
				md_printf(" (5) Command Ship              N/A    %6u%%  `bright green`complete`white`\r\n", player->command_ship);
			}
			else
			{
				md_printf(" (5) Command Ship %16uSPK %6u%%       %4s\r\n", 10000 * (player->command_ship + 1), player->command_ship, (player->sprockets >= 10000 * (player->command_ship + 1) ? "`bright green`yes`white`" : "`bright red` no`white`"));
			}
			md_printf(" (6) Colonize Planets                 %6u        ...\r\n", player->planets_ore + player->planets_food + player->planets_industrial + player->planets_military + player->planets_urban);
			md_printf(" (7) Food                      100    %6u     %6" PRIu64 "\r\n", player->food, player->credits / 100);
			md_printf(" (8) Spies                    5000    %6u     %6" PRIu64 "\r\n", player->spies, player->credits / 5000);
			md_printf("\r\n (9) Visit the Bank\r\n");
			md_printf(" (0) Disband Armies\r\n");
			md_printf("\r\n");
			md_printf(" (`bright green`D`white`) Done\r\n");
			md_printf("`bright green`============================================================`white`\r\n");

			c = md_get_answer("1234567890dD\r");
			switch (c)
			{
			case '1':
				md_printf("How many troops do you want to buy? (MAX `bright yellow`%" PRIu64 "`white`) ", player->credits / 100);
				
				i = gd_get_uint64(player->credits / 100);
				if (i * 100 > player->credits)
				{
					md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
				}
				else
				{
					player->troops += i;
					player->credits -= i * 100;
				}
				break;
			case '2':
				md_printf("How many generals do you want to buy? (MAX `bright yellow`%" PRIu64 "`white`) ", player->credits / 500);
				i = gd_get_uint64(player->credits / 500);
				if (i * 500 > player->credits)
				{
					md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
				}
				else
				{
					player->generals += i;
					player->credits -= i * 500;
				}
				break;
			case '3':
				md_printf("How many fighters do you want to buy? (MAX `bright yellow`%" PRIu64 "`white`) ", player->credits / 1000);
				i = gd_get_uint64(player->credits / 1000);
				if (i * 1000 > player->credits)
				{
					md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
				}
				else
				{
					player->fighters += i;
					player->credits -= i * 1000;
				}
				break;
			case '4':
				md_printf("How many defence stations do you want to buy? (MAX `bright yellow`%" PRIu64 "`white`) ", player->credits / 1000);
				i = gd_get_uint64(player->credits / 1000);

				if (i * 1000 > player->credits)
				{
					md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
				}
				else
				{
					player->defence_stations += i;
					player->credits -= i * 1000;
				}
				break;
			case '5':
				md_printf("Add to your command ship? (Y/`bright green`N`white`) ");
				c = md_get_answer("YyNn\r");

				if (c == 'y' || c == 'Y')
				{
					if (player->command_ship >= 100)
					{
						md_printf("\r\n`bright red`You can't construct any more!`white`\r\n");
					}
					else if ((player->command_ship + 1) * 10000 > player->sprockets)
					{
						md_printf("\r\n`bright red`You don't have enough sprockets!`white`\r\n");
					}
					else
					{
						player->command_ship++;
						player->sprockets -= player->command_ship * 10000;
					}
				}
				break;
			case '6':
				md_printf("What kind of planets do you want?\r\n");
				md_printf("  1. Ore        (You have: %u)\r\n", player->planets_ore);
				md_printf("  2. Food       (You have: %u)\r\n", player->planets_food);
				md_printf("  3. Soldier    (You have: %u)\r\n", player->planets_military);
				md_printf("  4. Industrial (You have: %u)\r\n", player->planets_industrial);
				md_printf("  5. Urban      (You have: %u)\r\n", player->planets_urban);
				c = md_get_answer("12345");
				switch (c)
				{
				case '1':
					md_printf("How many ore planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						i = strtoll(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " ore planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + (player->planets_ore + i) * 100);
							md_printf("You have %" PRIu64 " credits, go ahead? (Y/`bright green`N`white`) ", player->credits);
							c = md_get_answer("YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (player->credits < i * 2000 + (player->planets_ore + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									player->credits -= ((i * 2000) + ((player->planets_ore + i) * 100));
									player->planets_ore += i;
								}
							}
						}
					}
					break;
				case '2':
					md_printf("How many food planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						i = strtoll(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " food planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + (player->planets_food + i) * 100);
							md_printf("You have %u credits, go ahead? (Y/`bright green`N`white`) ", player->credits);
							c = md_get_answer("YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (player->credits < i * 2000 + (player->planets_food + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									player->credits -= ((i * 2000) + ((player->planets_food + i) * 100));
									player->planets_food += i;
								}
							}
						}
					}
					break;
				case '3':
					md_printf("How many military planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						i = strtoll(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " soldier planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + (player->planets_military + i) * 100);
							md_printf("You have %u credits, go ahead? (Y/`bright green`N`white`) ", player->credits);
							c = md_get_answer("YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (player->credits < i * 2000 + (player->planets_military + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									player->credits -= ((i * 2000) + ((player->planets_military + i) * 100));
									player->planets_military += i;
								}
							}
						}
					}
					break;
				case '4':
					md_printf("How many industrial planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						i = strtoll(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " industrial planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + (player->planets_industrial + i) * 100);
							md_printf("You have %u credits, go ahead? (Y/`bright green`N`white`) ", player->credits);
							c = md_get_answer("YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (player->credits < i * 2000 + (player->planets_industrial + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									player->credits -= ((i * 2000) + ((player->planets_industrial + i) * 100));
									player->planets_industrial += i;
								}
							}
						}
					}
					break;
				case '5':
					md_printf("How many urban planets do you want to aquire? ");
					md_getstring(buffer, 8, '0', '9');
					md_printf("\r\n");
					if (strlen(buffer) != 0)
					{
						i = strtoll(buffer, NULL, 10);
						if (i > 0)
						{
							md_printf("Exploration costs for %" PRId64 " urban planets amounts to %" PRIu64 " credits.\r\n", i, i * 2000 + (player->planets_urban + i) * 100);
							md_printf("You have %u credits, go ahead? (Y/`bright green`N`white`) ", player->credits);
							c = md_get_answer("YyNn\r");
							if (c == 'y' || c == 'Y')
							{
								if (player->credits < i * 2000 + (player->planets_urban + i) * 100)
								{
									md_printf("\r\n`bright red`You can't afford that!`white`\r\n");
								}
								else
								{
									player->credits -= ((i * 2000) + ((player->planets_urban + i) * 100));
									player->planets_urban += i;
								}
							}
						}
					}
					break;
				}

				break;
			case '7':
				md_printf("How much food do you want to buy? (MAX `bright yellow`%u`white`) ", player->credits / 100);
				i = gd_get_uint64(player->credits / 100);

				if (i * 100 > player->credits)
				{
					md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
				}
				else
				{
					player->food += i;
					player->credits -= i * 100;
				}
				break;
			case '8':
				md_printf("How many spies do you want to buy? (MAX `bright yellow`%u`white`) ", player->credits / 5000);
				i = gd_get_uint64(player->credits / 5000);
				if (i * 5000 > player->credits)
				{
					md_printf("\r\n`bright red`You can't afford that many!`white`\r\n");
				}
				else
				{
					player->spies += i;
					player->credits -= i * 5000;
				}
				break;
			case '9':
			{
				bank_done = 0;
				while (!bank_done)
				{
					if (player->bank_balance >= 0)
					{
						md_printf("Your current bank balance is `bright green`%" PRId64 " `white`credits.\r\nInterest is 0.1%% (%" PRId64 " credits) per day.\r\n", player->bank_balance, (int64_t)((long double)player->bank_balance * 0.001f));
					}
					else
					{
						md_printf("Your current bank balance is `bright red`%" PRId64 " `white`credits.\r\nInterest is 5%% (%" PRId64 " credits) per day.\r\n", player->bank_balance, labs((int64_t)((long double)player->bank_balance * 0.05f)));
					}
					md_printf("Your current allowed overdraft is %" PRIu64 " credits.\r\n", (calculate_score(player) * 100) / 2);
					md_printf("Would you like to (D)eposit, (W)ithdraw or (`bright green`L`white`)eave? ");
					c = md_get_answer("DdWwLl\r");
					switch (c)
					{
					case 'D':
					case 'd':
						md_printf("\r\n\r\nHow much would you like to deposit (0 - %" PRIu64 " credits) ? ", player->credits);
						i = gd_get_uint64(player->credits);
						if (i > player->credits)
						{
							md_printf("\r\n`bright red`You don't have that many credits!`white`\r\n");
						}
						else
						{
							player->credits -= i;
							player->bank_balance += i;
						}
						break;
					case 'W':
					case 'w':
						md_printf("\r\n\r\nHow much would you like to withdraw (0 - %" PRId64 " credits) ? ", player->bank_balance + ((calculate_score(player) * 100) / 2));
						i = gd_get_uint64(player->bank_balance + ((calculate_score(player) * 100) / 2));
						if (i > player->bank_balance + ((calculate_score(player) * 100) / 2))
						{
							md_printf("\r\n`bright red`You don't have that many credits!`white`\r\n");
						}
						else
						{
							player->bank_balance -= i;
							player->credits += i;
						}
						break;
					default:
						md_printf("\r\n\r\n");
						bank_done = 1;
						break;
					}
				}
			}
			break;
			case '0':
				md_printf("\r\n\r\n`bright red`Warning: `bright white`You gain no compensation for disbanding armies.`white`\r\n\r\n");
				if (player->troops > 0)
				{
					md_printf("Disband how many troops? (`bright green`0`white` - %u) ", player->troops);
					md_getstring(buffer, 10, '0', '9');
					if (strlen(buffer) != 0)
					{
						i = atoi(buffer);
						if (i > player->troops)
						{
							i = player->troops;
						}
						player->troops -= i;
					}
				}
				if (player->generals > 0)
				{
					md_printf("Disband how many generals? (`bright green`0`white` - %u) ", player->generals);
					md_getstring(buffer, 10, '0', '9');
					if (strlen(buffer) != 0)
					{
						i = atoi(buffer);
						if (i > player->generals)
						{
							i = player->generals;
						}
						player->generals -= i;
					}
				}
				if (player->fighters > 0)
				{
					md_printf("Disband how many fighters? (`bright green`0`white` - %u) ", player->fighters);
					md_getstring(buffer, 10, '0', '9');
					if (strlen(buffer) != 0)
					{
						i = atoi(buffer);
						if (i > player->fighters)
						{
							i = player->fighters;
						}
						player->fighters -= i;
					}
				}
				if (player->defence_stations > 0)
				{
					md_printf("Disband how many defence stations? (`bright green`0`white` - %u) ", player->defence_stations);
					md_getstring(buffer, 10, '0', '9');
					if (strlen(buffer) != 0)
					{
						i = atoi(buffer);
						if (i > player->defence_stations)
						{
							i = player->defence_stations;
						}
						player->defence_stations -= i;
					}
				}
				break;
			case '\r':
			case 'd':
			case 'D':
				done = 1;
				break;
			}
		}

		// covert operations
		if (player->spies > 0)
		{
			done = 0;
			while (!done)
			{
				md_printf("`bright magenta`============================================================\r\n");
				md_printf("`white` Covert Operations                 Spies: %u\r\n", player->spies);
				md_printf("`bright magenta`=============================`magenta`[`white`Price`magenta`]`bright magenta`========================\r\n");
				md_printf("`white` (1) Spy on someone nearby.....................1000\r\n");
				if (interBBSMode == 1)
				{
					md_printf("`white` (2) Long range reconnaissance................10000\r\n");
					md_printf("`white` (3) Sabotage Planets.........................10000\r\n");
				}
				md_printf(" (`bright green`D`white`) Done\r\n");
				md_printf("`bright magenta`============================================================`white`\r\n");
				c = md_get_answer("123dD\r");
				switch (tolower(c))
				{
				case '1':
					if (player->spies == 0)
					{
						md_printf("\r\n`bright red`You have no spies!\r\n");
						break;
					}
					if (player->credits < 1000)
					{
						md_printf("\r\n`bright red`You can't afford that!\r\n");
					}
					else
					{
						victim = select_victim(player, "Who do you want to spy on", 3);
						if (victim != NULL)
						{
							i = rand() % 100 + 1;
							player->credits -= 1000;
							if (i < 50)
							{
								md_printf("\r\nYour spy was caught and executed!\r\n");
								player->spies--;
								sprintf(message, "A spy from %s was caught trying to infiltrate your empire!", player->gamename);
								send_message(victim, NULL, message);
							}
							else
							{
								md_printf("\r\nYour spy was successful!\r\n");
								state_of_the_galaxy(victim);
							}
							free(victim);
							save_player(player);
						}
					}
					break;
				case '2':
					if (interBBSMode == 1)
					{
						if (player->spies == 0)
						{
							md_printf("\r\n`bright red`You have no spies!\r\n");
							break;
						}
						if (player->credits < 10000)
						{
							md_printf("\r\n`bright red`You can't afford that!\r\n");
						}
						else
						{
							addr = select_bbs(1);
							if (addr != 256)
							{
								memset(&msg, 0, sizeof(ibbsmsg_t));
								if (select_ibbs_player(addr, msg.victim_name) == 0)
								{

									player->spies--;
									player->credits -= 10000;

									msg.type = 8;
									msg.from = InterBBSInfo.myNode->nodeNumber;
									strcpy(msg.player_name, player->gamename);
									msg.created = time(NULL);
									msg.turns_in_protection = turns_in_protection;
									msg.turns_per_day = turns_per_day;
									msg2ne(&msg);
									if (IBSend(&InterBBSInfo, addr, &msg, sizeof(ibbsmsg_t)) != eSuccess)
									{
										player->spies++;
										player->credits += 10000;
										md_printf("Your spy failed to take off. Your forces have been returned.\r\n");
									}
									save_player(player);
								}
							}
						}
					}
					break;
				case '3':
					if (interBBSMode == 1)
					{
						if (player->spies == 0)
						{
							md_printf("\r\n`bright red`You have no spies!\r\n");
							break;
						}
						if (player->credits < 10000)
						{
							md_printf("\r\n`bright red`You can't afford that!\r\n");
						}
						else
						{
							addr = select_bbs(1);
							if (addr != 256)
							{
								memset(&msg, 0, sizeof(ibbsmsg_t));
								if (select_ibbs_player(addr, msg.victim_name) == 0)
								{
									while (1)
									{
										md_printf("\r\nSend how many spies? (MAX %u) ", player->spies);
										md_getstring(buffer, 8, '0', '9');
										if (strlen(buffer) > 0)
										{
											i = atoi(buffer);
											if (i > player->spies)
											{
												md_printf("\r\nYou don't have that many!\r\n");
											}
											else
											{
												md_printf("\r\nSending %" PRId64 " spies.\r\n", i);
												spies_to_send = i;
												break;
											}
										}
									}

									if (spies_to_send == 0)
									{
										md_printf("\r\n`bright red`You must send at least one spy!\r\n");
										break;
									}

									player->credits -= 10000;
									player->spies -= spies_to_send;

									msg.type = 9;
									msg.from = InterBBSInfo.myNode->nodeNumber;
									strcpy(msg.player_name, player->gamename);
									msg.created = time(NULL);
									msg.generals = spies_to_send;
									msg.turns_in_protection = turns_in_protection;
									msg.turns_per_day = turns_per_day;
									msg2ne(&msg);
									if (IBSend(&InterBBSInfo, addr, &msg, sizeof(ibbsmsg_t)) != eSuccess)
									{
										player->spies += spies_to_send;
										player->credits += 10000;
										md_printf("Your spies failed to take off. Your forces have been returned.\r\n");
									}
									save_player(player);
								}
							}
						}
					}
					break;
				case '\r':
				case 'd':
					done = 1;
					break;
				}
			}
		}
		// do you want to attack anyone
		md_printf("Do you want to launch an attack? (Y/`bright green`N`white`) ");
		c = md_get_answer("yYnN\r");

		if (tolower(c) == 'y')
		{
			if (player->total_turns < turns_in_protection)
			{
				md_printf("\r\nSorry, you are currently in protection and can not attack\r\n");
			}
			else
			{
				victim = select_victim(player, "Who do you want to attack", 2);
				if (victim != NULL)
				{
					// do attack

					troops_to_send = 0;

					if (player->troops > 0)
					{
						while (1)
						{
							md_printf("\r\nSend how many troops? (MAX %u) ", player->troops);
							i = gd_get_uint64(player->troops);
							if (i > player->troops)
							{
								md_printf("\r\nYou don't have that many!\r\n");
							}
							else if (i > 0)
							{
								md_printf("\r\nSending %" PRId64 " troops.\r\n", i);
								troops_to_send = i;
								player->troops -= i;
								break;
							}
							else if (i == 0)
							{
								md_printf("\r\nYou need at least 1 troop!\r\n");
								break;
							}
						}
					}
					else
					{
						md_printf("\r\nYou have no troops!\r\n");
						free(victim);
					}
					if (troops_to_send > 0)
					{
						if (player->generals > 0)
						{
							while (1)
							{
								md_printf("\r\nSend how many generals? (MAX %u) ", player->generals);
								i = gd_get_uint64(player->generals);
								if (i > player->generals)
								{
									md_printf("\r\nYou don't have that many!\r\n");
								}
								else
								{
									md_printf("\r\nSending %" PRId64 " generals.\r\n", i);
									generals_to_send = i;
									player->generals -= i;
									break;
								}
							}
						}
						else
						{
							generals_to_send = 0;
						}
						if (player->fighters > 0)
						{
							while (1)
							{
								md_printf("\r\nSend how many fighters? (MAX %u) ", player->fighters);
								i = gd_get_uint64(player->fighters);
								if (i > player->fighters)
								{
									md_printf("\r\nYou don't have that many!\r\n");
								}
								else
								{
									md_printf("\r\nSending %" PRId64 " fighters.\r\n", i);
									fighters_to_send = i;
									player->fighters -= i;
									break;
								}
							}
						}
						else
						{
							fighters_to_send = 0;
						}
						do_battle(victim, player, troops_to_send, generals_to_send, fighters_to_send);
						save_player(victim);
					}
					free(victim);
				}
			}
		}
		if (interBBSMode == 1)
		{
			md_printf("\r\nDo you want to launch an Inter-Galactic armada? (Y/`bright green`N`white`) ");
			c = md_get_answer("YyNn\r");
			if (tolower(c) == 'y')
			{
				if (player->total_turns < turns_in_protection)
				{
					md_printf("\r\nSorry, you are currently under protection and can not attack.\r\n");
				}
				else
				{
					addr = select_bbs(1);
					if (addr != 256)
					{
						memset(&msg, 0, sizeof(ibbsmsg_t));
						if (select_ibbs_player(addr, msg.victim_name) == 0)
						{
							msg.type = 2;
							msg.from = InterBBSInfo.myNode->nodeNumber;
							strcpy(msg.player_name, player->gamename);
							msg.score = 0;
							msg.plunder_credits = 0;
							msg.plunder_food = 0;
							msg.plunder_people = 0;
							msg.created = time(NULL);
							if (player->troops > 0)
							{
								while (1)
								{
									md_printf("\r\nSend how many troops? (MAX %u) ", player->troops);
									i = gd_get_uint64(player->troops);
									if (i > player->troops)
									{
										md_printf("\r\nYou don't have that many!\r\n");
									}
									else if (i > 0)
									{
										md_printf("\r\nSending %" PRId64 " troops.\r\n", i);
										msg.troops = i;
										player->troops -= i;
										break;
									}
									else
									{
										md_printf("\r\nYou must send at least 1 troop.\r\n");
									}
								}
							}
							else
							{
								md_printf("\r\nYou have no troops!\r\n");
							}
							if (msg.troops > 0)
							{
								if (player->generals > 0)
								{
									while (1)
									{
										md_printf("\r\nSend how many generals? (MAX %u) ", player->generals);
										i = gd_get_uint64(player->generals);
										if (i > player->generals)
										{
											md_printf("\r\nYou don't have that many!\r\n");
										}
										else
										{
											md_printf("\r\nSending %" PRId64 " generals.\r\n", i);
											msg.generals = i;
											player->generals -= i;
											break;
										}
									}
								}
								else
								{
									msg.generals = 0;
								}
								if (player->fighters > 0)
								{
									while (1)
									{
										md_printf("\r\nSend how many fighters? (MAX %u) ", player->fighters);
										i = gd_get_uint64(player->fighters);
										if (i > player->fighters)
										{
											md_printf("\r\nYou don't have that many!\r\n");
										}
										else
										{
											md_printf("\r\nSending %" PRId64 " fighters.\r\n", i);
											msg.fighters = i;
											player->fighters -= i;
											break;
										}
									}
								}
								else
								{
									msg.fighters = 0;
								}
								// send message
								msg.turns_in_protection = turns_in_protection;
								msg.turns_per_day = turns_per_day;
								msg2ne(&msg);
								if (IBSend(&InterBBSInfo, addr, &msg, sizeof(ibbsmsg_t)) != eSuccess)
								{
									player->troops += msg.troops;
									player->generals += msg.generals;
									player->fighters += msg.fighters;
									md_printf("Your armada failed to take off. Your forces have been returned.\r\n");
								}
							}
						}
					}
				}
			}
		}

		if (stat("ibbs_winner.dat", &st) != 0)
		{
			if (player->command_ship == 100 && player->troops >= 100000 && player->generals >= 10000 && player->fighters >= 10000)
			{
				md_printf("You have amassed a mighty force, launch an attack on `bright magenta`Queen Mapa`white`? (Y/`bright green`N) ");
				c = md_get_answer("YyNn\r");
				if (tolower(c) == 'y')
				{
					i = rand() % 100;

					if (i < 20)
					{
						// you win!
						md_printf("\r\n`bright green`You are victorious! Queen Mapa has been defeated\r\nand all the universe hails you as her heir!`white`");
						md_printf("`bright white`You have won the game!\r\n");
						game_won(player);
					}
					else
					{
						md_printf("\r\n`bright red`You are defeated. Your invasion force has been obliterated.`white`");

						player->command_ship = 0;
						player->troops -= 100000;
						player->generals -= 10000;
						player->fighters -= 10000;
					}
				}
			}
		}
		md_printf("\r\n\r\n");

		// Productions

		if (player->planets_food > 0)
		{
			md_printf("Your food planets produced `bright white`%u `white`food.\r\n", 10 * player->planets_food);

			player->food += 10 * player->planets_food;
		}

		if (player->planets_military > 0)
		{
			md_printf("Your soldier planets provided `bright white`%u`white` troops\r\n", player->planets_military * 10);

			player->troops += 10 * player->planets_military;
		}

		if (player->planets_ore > 0)
		{
			if (player->planets_ore > (uint32_t)(player->population / 25.f))
			{
				total_ore = (uint32_t)(player->population / 25.f * 100.f);
			}
			else
			{
				total_ore = player->planets_ore * 100;
			}

			md_printf("Your ore planets mined `bright white`%u`white` ore.\r\n", total_ore);

			player->ore += total_ore;
		}

		if (player->planets_industrial > 0)
		{

			if (player->planets_industrial > (uint32_t)(player->population / 25.f))
			{
				total_industrial = (uint32_t)(player->population / 25.f * 100.f);
			}
			else
			{
				total_industrial = player->planets_industrial * 100;
			}
			if (total_industrial > player->ore)
			{
				total_industrial = player->ore;
			}

			player->ore -= total_industrial;

			md_printf("Your industrial planets produced `bright white`%u`white` sprockets leaving you with `bright white`%u`white` ore.\r\n", total_industrial, player->ore);

			player->sprockets += total_industrial;
		}
		md_printf("Taxes produce `bright white`%u`white` credits\r\n", player->population * 23);

		player->credits += player->population * 23;

		// Population Changes
		if (starvation < 1)
		{
			md_printf("`bright red`%u `white`citizens died from starvation\r\n", (uint32_t)(player->population - ((float)player->population * starvation)));
			player->population -= player->population - (player->population * starvation);
		}
		else
		{
			if (player->population > player->planets_urban * 50)
			{
				md_printf("`bright yellow`No population increase due to not enough urban planets`white`\r\n");
			}
			else
			{
				md_printf("`bright white`%u `white`new citizens join the empire\r\n", (uint32_t)((float)player->population * 0.05));
				player->population += player->population * 0.05;
			}
		}

		if (loyalty < 1)
		{
			md_printf("`bright red`%u `white`troops fled the empire\r\n", (uint32_t)(player->troops - ((float)player->troops * loyalty)));
			player->troops -= player->troops - (player->troops * loyalty);
		}

		do_lua_script("events");

		// loop
		player->turns_left--;
		player->total_turns++;
		save_player(player);

		if (player->turns_left > 0)
		{
			md_printf("\r\n`bright yellow`Continue`white` ? (`bright green`Y`white`/N) ");
			c = md_get_answer("YyNn\r");
			if (tolower(c) == 'n')
			{
				md_printf("\r\n");
				break;
			}
		}
	}
	if (player->turns_left == 0)
	{
		md_printf("You're out of turns for today, please come back tomorrow.\r\n");
		md_printf("\r\nPress a key to continue\r\n");
		md_getc();
	}

	// show scores
	md_sendfile("scores.ans", TRUE);
	md_printf("\r\nPress a key to continue\r\n");
	md_getc();
}

void door_quit(void)
{
	if (full == 1)
	{
		perform_maintenance();
	}
	if (unlink("inuse.flg") != 0)
	{
		perror("unlink ");
	}
}

void lua_push_cfunctions(lua_State *L)
{
	lua_pushcfunction(L, lua_getTroops);
	lua_setglobal(L, "gd_get_troops");
	lua_pushcfunction(L, lua_getGenerals);
	lua_setglobal(L, "gd_get_generals");
	lua_pushcfunction(L, lua_getFighters);
	lua_setglobal(L, "gd_get_fighters");
	lua_pushcfunction(L, lua_getDefenceStations);
	lua_setglobal(L, "gd_get_defence_stations");
	lua_pushcfunction(L, lua_getSpies);
	lua_setglobal(L, "gd_get_spies");
	lua_pushcfunction(L, lua_getPopulation);
	lua_setglobal(L, "gd_get_population");
	lua_pushcfunction(L, lua_getFood);
	lua_setglobal(L, "gd_get_food");
	lua_pushcfunction(L, lua_getCredits);
	lua_setglobal(L, "gd_get_credits");
	lua_pushcfunction(L, lua_setTroops);
	lua_setglobal(L, "gd_set_troops");
	lua_pushcfunction(L, lua_setGenerals);
	lua_setglobal(L, "gd_set_generals");
	lua_pushcfunction(L, lua_setFighters);
	lua_setglobal(L, "gd_set_fighters");
	lua_pushcfunction(L, lua_setDefenceStations);
	lua_setglobal(L, "gd_set_defence_stations");
	lua_pushcfunction(L, lua_setSpies);
	lua_setglobal(L, "gd_set_spies");
	lua_pushcfunction(L, lua_setPopulation);
	lua_setglobal(L, "gd_set_population");
	lua_pushcfunction(L, lua_setFood);
	lua_setglobal(L, "gd_set_food");
	lua_pushcfunction(L, lua_setCredits);
	lua_setglobal(L, "gd_set_credits");
	lua_pushcfunction(L, lua_printYellow);
	lua_setglobal(L, "gd_print_yellow");
	lua_pushcfunction(L, lua_printGreen);
	lua_setglobal(L, "gd_print_green");
	lua_pushcfunction(L, lua_printRed);
	lua_setglobal(L, "gd_print_red");
	lua_pushcfunction(L, lua_getPlanets);
	lua_setglobal(L, "gd_get_planets");
	lua_pushcfunction(L, lua_destroyPlanets);
	lua_setglobal(L, "gd_destroy_planets");
	lua_pushcfunction(L, lua_inProtection);
	lua_setglobal(L, "gd_in_protection");
}

int main(int argc, char **argv)
{
	char bbsname[256];
	time_t timenow;
	struct tm today_tm;
	struct tm last_tm;
	struct tm *ptr;
	char c;
	int i;
	struct stat s;
	int inuse = 0;
	FILE *fptr;
	uint32_t newgameid;
	ibbsmsg_t msg;
	int days_passed;
	FILE *fptr2;
	char message[256];
	full = 0;

	srand(time(NULL));


	turns_per_day = TURNS_PER_DAY;
	turns_in_protection = TURNS_IN_PROTECTION;

	InterBBSInfo.myNode = (tOtherNode *)malloc(sizeof(tOtherNode));
	if (InterBBSInfo.myNode == NULL)
	{
		fprintf(stderr, "Out of memory!\n");
		exit(-1);
	}

	log_path = NULL;
	bad_path = NULL;
	delete_bad = 0;

	if (ini_parse("galactic.ini", handler, NULL) < 0)
	{
		fprintf(stderr, "Unable to load galactic.ini\n");
	}

	if (interBBSMode == 1)
	{
		IBSetLogger(dolog);
		if (IBReadConfig(&InterBBSInfo, FILEEXT "-IBBS.CFG") != eSuccess)
		{
			interBBSMode = 0;
		}
	}

	if (stat("inuse.flg", &s) == 0)
	{
		inuse = 1;
	}

	if (argc < 2) {
		fprintf(stderr, "usage:\n%s (DROPFILE [SOCKET] [/full] | (maintenance | /reset GAMEID | /add NODENO, BBS NAME | /del NODENO))\n", argv[0]);
		return 0;
	}

	if (argc > 1 && strcasecmp(argv[1], "maintenance") == 0)
	{
		if (inuse == 1)
		{
			fprintf(stderr, "Game currently in use.\n");
			return 2;
		}
		else
		{
			fptr = fopen("inuse.flg", "w");
			if (!fptr)
			{
				fprintf(stderr, "Unable to create inuse.flg, Check permissions!\n");
				return -1;
			}
			fputs("INUSE!", fptr);
			fclose(fptr);
			perform_maintenance();
			if (unlink("inuse.flg") != 0)
			{
				perror("unlink ");
			}
			return 0;
		}
	}

	if (argc > 2 && (strcasecmp(argv[1], "-ADD") == 0 || strcasecmp(argv[1], "/ADD") == 0))
	{
		printf("Adding Link %d (%s)\n", atoi(argv[2]), argv[3]);

		memset(&msg, 0, sizeof(ibbsmsg_t));
		msg.type = 5;
		msg.from = InterBBSInfo.myNode->nodeNumber;
		sprintf(msg.player_name, "%s", argv[2]);
		strcpy(msg.victim_name, "ADD");
		sprintf(msg.message, "%s", argv[3]);
		msg.created = time(NULL);
		msg.turns_per_day = turns_per_day;
		msg.turns_in_protection = turns_in_protection;
		msg2ne(&msg);
		IBSendAll(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));
		remove_link(atoi(argv[2]));
		add_link(atoi(argv[2]), argv[3]);
		return 0;
	}

	if (argc > 1 && (strcasecmp(argv[1], "-DEL") == 0 || strcasecmp(argv[1], "/DEL") == 0))
	{
		printf("Removing Link %d\n", atoi(argv[2]));
		memset(&msg, 0, sizeof(ibbsmsg_t));

		msg.type = 5;
		msg.from = InterBBSInfo.myNode->nodeNumber;
		sprintf(msg.player_name, "%s", argv[2]);
		strcpy(msg.victim_name, "REMOVE");
		msg.created = time(NULL);
		msg.turns_per_day = turns_per_day;
		msg.turns_in_protection = turns_in_protection;
		msg2ne(&msg);
		IBSendAll(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));
		remove_link(atoi(argv[2]));
		return 0;
	}

	if (argc > 1 && (strcasecmp(argv[1], "-RESET") == 0 || strcasecmp(argv[1], "/RESET") == 0))
	{
		memset(&msg, 0, sizeof(ibbsmsg_t));
		newgameid = strtoul(argv[2], NULL, 10);

		if (newgameid < 1)
		{
			fprintf(stderr, "Invalid game id!\n");
			return -1;
		}

		msg.type = 7;
		msg.from = InterBBSInfo.myNode->nodeNumber;
		sprintf(msg.player_name, "%d", newgameid);
		msg.created = time(NULL);
		msg.turns_per_day = turns_per_day;
		msg.turns_in_protection = turns_in_protection;
		msg2ne(&msg);
		IBSendAll(&InterBBSInfo, &msg, sizeof(ibbsmsg_t));

		fptr = fopen(FILEEXT "-IBBS.CFG", "r");
		if (!fptr)
		{
			return -1;
		}

		fptr2 = fopen(FILEEXT "-IBBS.CFG.BAK", "w");
		if (!fptr2)
		{
			fclose(fptr);
			return -1;
		}
		fgets(message, 256, fptr);
		while (!feof(fptr))
		{
			fputs(message, fptr2);
			fgets(message, 256, fptr);
		}
		fclose(fptr2);
		fclose(fptr);

		fptr = fopen(FILEEXT "-IBBS.CFG.BAK", "r");
		if (!fptr)
		{
			return -1;
		}

		fptr2 = fopen(FILEEXT "-IBBS.CFG", "w");
		if (!fptr2)
		{
			return -1;
		}

		fgets(message, 256, fptr);
		while (!feof(fptr))
		{
			if (strncasecmp(message, "GameID", 6) == 0)
			{
				fprintf(fptr2, "GameID %d\n", newgameid);
			}
			else
			{
				fputs(message, fptr2);
			}
			fgets(message, 256, fptr);
		}
		fclose(fptr2);
		fclose(fptr);
		unlink(FILEEXT "-IBBS.CFG.BAK");

#if defined(_MSC_VER) || defined(WIN32)
        system("reset.bat");
#else
        system("./reset.sh");
#endif

		return 0;
	}

	for (i = 2; i < argc; i++)
	{

		if (strcasecmp(argv[i], "/full") == 0 || strcasecmp(argv[i], "-full") == 0)
		{
			full = 1;
			break;
		}
	}

	md_init(argv[1], (i < 3 ? -1 : atoi(argv[2])));

	if (inuse == 1)
	{
		md_printf("Sorry, the game is currently in use. Please try again later.\r\n");
		md_getc();
		md_exit(2);
	}

	atexit(door_quit);
	fptr = fopen("inuse.flg", "w");
	if (!fptr)
	{
		fprintf(stderr, "Unable to open inuse.flg! Check permissions!\n");
		md_exit(-1);
	}
	fputs("INUSE!", fptr);
	fclose(fptr);
	md_sendfile("logo.ans", FALSE);
	md_getc();
	snprintf(bbsname, 255, "%s %s!%s", mdcontrol.user_firstname, mdcontrol.user_lastname, mdcontrol.user_alias);

	gPlayer = load_player(bbsname);
	if (gPlayer == NULL)
	{
		gPlayer = new_player(bbsname);
		if (gPlayer == NULL)
		{
			md_exit(0);
			exit(0);
		}
		else
		{
			save_player(gPlayer);
			gPlayer = load_player(bbsname);
		}
	}

	ptr = localtime(&gPlayer->last_played);
	memcpy(&last_tm, ptr, sizeof(struct tm));
	timenow = time(NULL);
	ptr = localtime(&timenow);
	memcpy(&today_tm, ptr, sizeof(struct tm));

	days_passed = 0;

	if (today_tm.tm_yday != last_tm.tm_yday || today_tm.tm_year != last_tm.tm_year)
	{
		if (today_tm.tm_year != last_tm.tm_year)
		{
			days_passed = 365 - last_tm.tm_yday + today_tm.tm_yday;
		}
		else
		{
			days_passed = today_tm.tm_yday - last_tm.tm_yday;
		}

		gPlayer->turns_left = turns_per_day;
	}
	gPlayer->last_played = timenow;

	do
	{
		md_printf("\r\n`white`Game Menu (v%d.%d.%d-%s)\r\n", VERSION_MAJOR, VERSION_MINOR, VERSION_MICRO, VERSION_TYPE);
		md_printf("`red`--------------------------------------\r\n");
		md_printf(" `white`(`bright yellow`1`white`) Play Game\r\n");
		md_printf(" `white`(`bright yellow`2`white`) See Messages\r\n");
		md_printf(" `white`(`bright yellow`3`white`) See Status\r\n");
		md_printf(" `white`(`bright yellow`4`white`) See Scores\r\n");
		if (interBBSMode == 1)
		{
			md_printf(" `white`(`bright yellow`5`white`) See InterBBS Nodes\r\n");
			md_printf(" `white`(`bright yellow`6`white`) See InterBBS Scores\r\n");
		}
		md_printf(" `white`(`bright yellow`Q`white`) Exit Game\r\n");
		md_printf("`red`--------------------------------------`white`\r\n");
		md_printf("Your Choice? ");
		if (interBBSMode == 1)
		{
			c = md_get_answer("123456qQ");
		}
		else
		{
			c = md_get_answer("1234qQ");
		}
		md_printf("\r\n");
		switch (c)
		{
		case '1':
			if (days_passed > 0)
			{
				md_printf("\r\n\r\nIt's been %d days since you last played!\r\n", days_passed);
				if (gPlayer->bank_balance > 0)
				{
					md_printf("You've earned %" PRId64 " credits in interest on your bank balance!\r\n", (int64_t)((long double)gPlayer->bank_balance * 0.001f * (long double)days_passed));
					gPlayer->bank_balance += (int64_t)((long double)gPlayer->bank_balance * 0.001f * (long double)days_passed);
				}
				if (gPlayer->bank_balance < 0)
				{
					md_printf("You've been charged %" PRId64 " credits in interest on your bank balance!\r\n", labs((int64_t)((long double)gPlayer->bank_balance * 0.05f * (long double)days_passed)));
					gPlayer->bank_balance += (int64_t)((long double)gPlayer->bank_balance * 0.05f * (long double)days_passed);
				}
			}
			game_loop(gPlayer);
			days_passed = 0;
			break;
		case '2':
			unseen_msgs(gPlayer);
			if (interBBSMode == 1)
			{
				unseen_ibbs_msgs(gPlayer);
			}
			break;
		case '3':
			state_of_the_galaxy(gPlayer);
			md_printf("\r\nPress a key to continue\r\n");
			md_getc();
			break;
		case '4':
			md_sendfile("scores.ans", TRUE);
			md_printf("\r\nPress a key to continue\r\n");
			md_getc();
			break;
		case '5':
			for (i = 0; i < InterBBSInfo.otherNodeCount; i++)
			{
				md_printf("`bright green`%s`white`\r\n", InterBBSInfo.otherNodes[i]->name);
			}
			md_printf("\r\nPress a key to continue\r\n");
			md_getc();
			break;
		case '6':
			md_sendfile("ibbs_scores.ans", TRUE);
			md_printf("\r\nPress a key to continue\r\n");
			md_getc();
			break;
		default:
			break;
		}
	} while (tolower(c) != 'q');
	free(gPlayer);
	md_exit(0);
}
