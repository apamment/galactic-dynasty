#!/bin/sh

if [ -e users.db3 ]
then
    rm users.db3
fi

if [ -e msgs.db3 ]
then
    rm msgs.db3
fi

if [ -e interbbs.db3 ]
then
    rm interbbs.db3
fi

if [ -e ibbs_info.db ]
then
	rm ibbs_info.db
fi

if [ -e ibbs_scores.ans ]
then
	rm ibbs_scores.ans
fi

if [ -e scores.ans ]
then
        rm scores.ans
fi

if [ -e game_id.dat ]
then
	rm game_id.dat
fi

if [ -e ibbs_winner.dat ]
then
	rm ibbs_winner.dat
fi

if [ -e ibbs_scores.asc ]
then
	rm ibbs_scores.asc
fi

if [ -e scores.asc ]
then
        rm scores.asc
fi
